# RealMD
Real Material Design with DoIt.JS

## DoIt.JS
Simple, clever, cool. Just do it!

---

### IO

`DoIt.IO` module

#### SSE
Server-Sent Events implementation.

##### Methods

##### `constructor(url:string, external:boolean = false)`
Creates the instance of the SSE class with the given `url`, but doesn't connect to it. The `external` variable should be
true if the `url` is on an external path (i.e. CORS is needed). A path is external if any of the following parameters
doesn't match with the document's URL:
- scheme - *http, https*, etc.
- (sub)domain
- port

##### `connect()`
Connects to the server.

##### `close()`
Closes the connection to the server.

##### `listen(type:string, callback:Function)`
Listens to the `type` and calls `callback` with the *event* if received.
See [this](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events#Fields) document
for the possible properties of the received *event*.

##### `unlisten(callback:Function)`
Removes the `callback` registered with `listen(...)`. Note that it must be the same callback method that was registered
before otherwise it will not remove it!

##### `onOpen(callback:Function)`
Convenience method for registering listeners to `open` events.

##### `onMessage(callback:Function)`
Convenience method for registering listeners to `message` events.

##### `onClose(callback:Function)`
Convenience method for registering listeners to `close` events.

##### `onError(callback:Function)`
Convenience method for registering listeners to `error` events.

##### `isAvailable()`
Returns `true` if SSE is supported by the browser, otherwise `false``.

#### WebSocket
*Coming soon...*

---

### Notifications

Notification API implementation.

See more at
[https://developer.mozilla.org/en-US/docs/Web/API/notification](https://developer.mozilla.org/en-US/docs/Web/API/notification)

#### Methods

##### `init([callback:Function])`
Requests permission from the user to display notifications and calls the optional `callback` method with the result.

##### `notify(title:string, message:string, [...])`
Displays a notification for the user.
Rest of the params are TBD.

---

### PageVisibility

Page Visibility API implementation.

See more at
[https://developer.mozilla.org/en-US/docs/Web/API/Page_Visibility_API](https://developer.mozilla.org/en-US/docs/Web/API/Page_Visibility_API)

#### Methods

##### `listen(callback:Function)`
Adds a listener to page visibility changes.

##### `unlisten(callback:Function)`
Removes a listener. Note that it must be the same callback method that was registered before otherwise it will not
remove it!

##### `clear()`
Removes all the listeners.

##### `isVisible()`
Returns `true` if the page is currently visible to the user, otherwise `false`.

---

### Elements

`DoIt.Elements` module

#### Methods

The methods can be chained.

##### `register(name:string, element:Element|any)`
Registers the given `name` and sets the `element` for it. This `element` will be used as the base of instantiation
for DOM elements, thus it *should not* contain static properties.

---

### Services

`DoIt.Services` module

#### Methods

The methods can be chained.

##### `register(name:string, service:Service|typeof Service|IService)`
Registers the given `name` and sets the `service` for it. This `service` will be instantiated only once so other
parts of the application access the same one making it suitable to be used as an inter-element communicator.

##### `get(name:string)`
Retrieves the service with the given `name` registered via `register(...)`.

---

### Routing

`DoIt.Routing` module

#### Methods

The methods can be chained.

##### `route(path:string, controller:PathController)`
Registers the given `path` and sets the `controller` for it. The controller here refers to a `PathController` object
that controls the path settings, like the page's URL that should be loaded, the AJAX settings, and the name of the
`PageController` that will be instantiated and used as the context for the loaded page.

The path can contain parameters in the following format: `{paramName[:type]}`. The type is optional, by default it
accepts anything. The possible parameter types are below.

###### PathController

- `url` - the URL of the page that should be loaded; this can also contain the parameter names in
`{paramName`} format (*note the missing type argument*)
- `settings` - the AJAX settings; see
[the jQuery AJAX settings documentation](http://api.jquery.com/jQuery.ajax/#jQuery-ajax-settings) for more details
- `controller` - the name of the controller that will be instantiated and used as the context of the page

###### Possible parameter types

- `int` - integer
- `number` - decimal number
- `word` - alphanumeric word that can contain the following characters: a-z, A-Z, 0-9, _ (underscore)
- `any` - **default**, accepts anything (*except empty strings*)

The order is from the strictest (`int`) to the most tolerant (`any`).

###### Path examples
- `/page/{id:int}`
- `/page/the-id-is-{id:int}`
- `/search/{query}`
- `/search/dir-{id:int}/{query}`

**Note that the order of registration sets the matching order too.** Consider the following registration order:
- `/page/{this_can_be_anything}`
- `/page/{id:int}`

In this scenario, the second rule will never be matching as the first one matches anything. If you have such rules, make
sure that the strictest is registered first and then the others (*see the possible types' list above*).

##### `page(name: string, controller: PageController|function)`
Registers the given `name` as the name of the `controller` that will be the context of the page which is loaded.

The controller has a simple and an advanced mode. Both provide the supplied parameters by retrieving `this.params`
inside the function or the object.

###### Simple
The controller is a function that receives the following parameters:

- `response` - the retrieved data (*converted to JSON if the data is JSON and the server sends the appropriate headers*)
or the raw error message in case of failure
- `isSuccessful` - `true` if the request was successful, otherwise `false`
- `jqXHR` - the jQuery XmlHttpRequest object, see [the jQuery documentation](http://api.jquery.com/jQuery.ajax/#jqXHR)
for more details

###### Advanced
The controller is an object with the following (*optional*) methods:

##### `boolean before(url:string)`
Called before the actual request and gives a chance to change the parameters (`this.params`) and/or stop the request by
returning `false`.

##### `void done(response:any, jqXHR:JQueryXHR)`
Called if the request was successful.

- `response` - the retrieved data (*converted to JSON if the data is JSON and the server sends the appropriate headers*)
or the raw error message in case of failure
- `jqXHR` - the jQuery XmlHttpRequest object, see [the jQuery documentation](http://api.jquery.com/jQuery.ajax/#jqXHR)
for more details

##### `void fail(response:any, jqXHR:JQueryXHR)`
Called if the request was not successful. The status code can be accessed through the `jqXHR` object's `status`
property (`jqXHR.status`).

- `response` - the retrieved data (*converted to JSON if the data is JSON and the server sends the appropriate headers*)
or the raw error message in case of failure
- `jqXHR` - the jQuery XmlHttpRequest object, see [the jQuery documentation](http://api.jquery.com/jQuery.ajax/#jqXHR)
for more details


#### Example routing
```javascript
DoIt.Routing
    .route('/page/{id:int}', {
        url: 'pages/page{id}.html',
        controller: 'PageController'
    })
    .route('/page/{query}', {
        url: 'pages/page2.html',
        settings: { // custom jQuery AJAX settings
            method: 'POST'
        },
        controller: 'PageWordController'
    })
    // a detailed object
    .page('PageController', {
        before: function (url) {
            // sent parameters can be accessed through this.params
            console.log('params:', this.params);
            // [...]
        },
        done: function (response, jqXHR) {
            // sent parameters can be accessed through this.params
            console.log('params:', this.params);
            // [...]
        },
        fail: function (response, jqXHR) {
            // sent parameters can be accessed through this.params
            console.log('params:', this.params);
            // [...]
        }
    })
    // a simple function
    .page('PageWordController', function (response, isSuccessful, jqXHR) {
        // sent parameters can be accessed through this.params
        console.log('params:', this.params);
        // [...]
    });
```
