var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../js/scroptjs/typescript/scropt.ts" />
var Material;
(function (Material) {
    var FloatingInput = (function (_super) {
        __extends(FloatingInput, _super);
        function FloatingInput() {
            _super.apply(this, arguments);
            this.replace = scropt.Elements.InsertMode.None;
        }
        FloatingInput.prototype.render = function (match) {
            var ph = $(match).attr('placeholder');
            var val = $(match).val();
            if (ph) {
                $(match).removeAttr('placeholder');
                $(match).wrap("<span class=\"material-input-floating\" data-title=\"" + ph + "\"></span>");
                $(match).focus(function () {
                    $(this).parent().removeClass('material-input-floating-default').addClass('material-input-floating-active');
                }).blur(function () {
                    if ($(this).val() === "") {
                        $(this).parent().removeClass('material-input-floating-active');
                    }
                    else {
                        $(this).parent().addClass('material-input-floating-default');
                    }
                });
                $(match).on('input', function () {
                    if ($(this).val() === "") {
                        $(this).parent().removeClass('material-input-floating-default material-input-floating-active');
                    }
                    else {
                        $(this).parent().addClass('material-input-floating-active material-input-floating-default');
                    }
                });
            }
            if (val && val !== "") {
                $(match).parent().addClass('material-input-floating-active material-input-floating-default');
            }
            var errorMsg = $(match).attr('data-error');
            if (errorMsg) {
                $(this).after($('<div class="material-input-msg-error">' + errorMsg + '</div>'));
            }
            return undefined;
        };
        FloatingInput.selector = "input[placeholder]:not([placeholder=\"\"]):not(.material-clone)";
        return FloatingInput;
    }(scropt.Elements.Element));
    Material.FloatingInput = FloatingInput;
    var Checkbox = (function (_super) {
        __extends(Checkbox, _super);
        function Checkbox() {
            _super.apply(this, arguments);
            this.replace = scropt.Elements.InsertMode.Replace;
        }
        Checkbox.prototype.render = function (match) {
            var elem = $("<span class=\"material-checkbox\"></span>");
            scropt.Elements.copyAttributes(match, elem);
            this.handleScropt(elem);
            var fnClk = function () {
                if (elem.attr('disabled')) {
                    return;
                }
                var isChecked = !$(elem).attr('checked');
                if (isChecked) {
                    $(elem).attr('checked', 'checked');
                }
                else {
                    $(elem).removeAttr('checked');
                }
                elem.trigger('change');
            };
            if ($(elem).attr('tabindex') === undefined) {
                $(elem).attr('tabindex', 0);
            }
            $(elem).off('click').click(function () {
                $(this).removeAttr('data-tabbed');
                fnClk();
            });
            $(elem).off('keydown').keydown(function (event) {
                var code = event.which || event.keyCode;
                switch (code) {
                    case 13:
                    case 32:
                        fnClk();
                        break;
                }
            });
            $(elem).keyup(function (event) {
                var code = event.which || event.keyCode;
                if (code === 9) {
                    $(this).attr('data-tabbed', 'true');
                }
            });
            $(elem).next('label').off('click').click(function () {
                elem.click();
            });
            return elem;
        };
        Checkbox.prototype.handleScropt = function (elem) {
            var varstore = new scropt.VarStoreImpl(this.binding);
            var attrs = scropt.DataBinding.getScroptAttributes(elem);
            for (var attr in attrs) {
                switch (attr) {
                    case 'model':
                        this.handleModel(attrs[attr], elem);
                        break;
                    case 'disabled':
                        this.handleDisabled(attrs[attr], elem, varstore);
                        break;
                }
            }
            elem.addClass(scropt.SC_CLASS_UPGRADED);
        };
        Checkbox.prototype.handleModel = function (path, jqElem) {
            var model = this.binding;
            var defValue = model.get(path);
            if (defValue == Checkbox.getValue(jqElem)) {
                jqElem.attr('checked', 'checked');
            }
            else {
                jqElem.removeAttr('checked');
            }
            //jqElem.trigger('change');
            model.observe(path, function (type, target, prop, newValue) {
                if (newValue == Checkbox.getValue(jqElem)) {
                    jqElem.attr('checked', 'checked');
                }
                else {
                    jqElem.removeAttr('checked');
                }
                //jqElem.trigger('change');
            });
            jqElem.on('change', function (e) {
                if (!e.originalEvent) {
                    if (!!jqElem.attr('checked')) {
                        model.update(path, Checkbox.getValue(jqElem));
                    }
                    else {
                        model.update(path, undefined);
                    }
                }
            });
        };
        Checkbox.prototype.handleDisabled = function (attr, jqElem, varstore) {
            var result = scropt.DataBinding.parse('disabled', attr, varstore);
            if (result !== null) {
                switch (result.type) {
                    case scropt.DataBinding.ParsedType.VAR_NAME:
                        Checkbox.setDisabled(jqElem, !!varstore.getModel().get(attr));
                        varstore.getModel().observe(attr, function (type, target, prop, newValue) {
                            Checkbox.setDisabled(jqElem, !!newValue);
                        });
                        varstore.clearWatched();
                        break;
                    case scropt.DataBinding.ParsedType.BOOLEAN:
                        Checkbox.setDisabled(jqElem, result.value);
                        var watched = varstore.getWatched();
                        if (watched) {
                            watched.forEach(function (val) {
                                varstore.getModel().observe(val, function (type, target, prop, newValue) {
                                    var newRes = scropt.DataBinding.parseDirect(result.type, attr, varstore);
                                    if (newRes != null) {
                                        Checkbox.setDisabled(jqElem, newRes.value);
                                    }
                                    varstore.clearWatched();
                                });
                            });
                        }
                        break;
                }
            }
            varstore.clearWatched();
        };
        Checkbox.getValue = function (elem) {
            return elem.attr('data-value') || elem.attr('value');
        };
        Checkbox.setDisabled = function (jqElem, disabled) {
            if (disabled) {
                jqElem.attr('disabled', 'disabled');
            }
            else {
                jqElem.removeAttr('disabled');
            }
        };
        Checkbox.selector = "input[type=\"checkbox\"]:not(.material-clone)"; //`.material-checkbox:not(.material-clone)`;
        return Checkbox;
    }(scropt.Elements.Element));
    Material.Checkbox = Checkbox;
    var RadioButton = (function (_super) {
        __extends(RadioButton, _super);
        function RadioButton() {
            _super.apply(this, arguments);
            this.replace = scropt.Elements.InsertMode.Replace;
        }
        //parents = "fieldset";
        RadioButton.prototype.render = function (match) {
            var elem = $("<span class=\"material-radio\"></span>");
            scropt.Elements.copyAttributes(match, elem);
            this.handleScropt(elem);
            var fnClk = function (e) {
                if (elem.attr('disabled')) {
                    return;
                }
                //group.find('.material-radio[checked]').attr('checked', false);
                $(elem).attr('checked', 'checked');
                elem.trigger('change');
                /*var fnChange = $(group).data('change');
                 if (fnChange) {
                 fnChange.apply(group, e);
                 }*/
            };
            if ($(elem).attr('tabindex') === undefined) {
                $(elem).attr('tabindex', 0);
            }
            $(elem).off('click').click(function (e) {
                $(this).removeAttr('data-tabbed');
                fnClk(e);
            });
            $(elem).off('keydown').keydown(function (event) {
                var code = event.which || event.keyCode;
                switch (code) {
                    case 13:
                    case 32:
                        fnClk(event);
                        break;
                }
            });
            $(elem).keyup(function (event) {
                var code = event.which || event.keyCode;
                if (code === 9) {
                    $(this).attr('data-tabbed', 'true');
                }
            });
            $(elem).next('label').off('click').click(function () {
                elem.click();
            });
            return elem;
        };
        RadioButton.prototype.handleScropt = function (elem) {
            var varstore = new scropt.VarStoreImpl(this.binding);
            var attrs = scropt.DataBinding.getScroptAttributes(elem);
            for (var attr in attrs) {
                switch (attr) {
                    case 'model':
                        this.handleModel(attrs[attr], elem);
                        break;
                    case 'disabled':
                        this.handleDisabled(attrs[attr], elem, varstore);
                        break;
                }
            }
            elem.addClass(scropt.SC_CLASS_UPGRADED);
        };
        RadioButton.prototype.handleModel = function (path, jqElem) {
            var model = this.binding;
            var defValue = model.get(path);
            if (defValue == RadioButton.getValue(jqElem)) {
                jqElem.attr('checked', 'checked');
            }
            else {
                jqElem.removeAttr('checked');
            }
            //jqElem.trigger('change');
            model.observe(path, function (type, target, prop, newValue) {
                if (newValue == RadioButton.getValue(jqElem)) {
                    jqElem.attr('checked', 'checked');
                }
                else {
                    jqElem.removeAttr('checked');
                }
                //jqElem.trigger('change');
            });
            jqElem.on('change', function (e) {
                if (!e.originalEvent) {
                    if (!!jqElem.attr('checked')) {
                        model.update(path, RadioButton.getValue(jqElem));
                    }
                    else {
                        model.update(path, undefined);
                    }
                }
            });
        };
        RadioButton.prototype.handleDisabled = function (attr, jqElem, varstore) {
            var result = scropt.DataBinding.parse('disabled', attr, varstore);
            if (result !== null) {
                switch (result.type) {
                    case scropt.DataBinding.ParsedType.VAR_NAME:
                        RadioButton.setDisabled(jqElem, !!varstore.getModel().get(attr));
                        varstore.getModel().observe(attr, function (type, target, prop, newValue) {
                            RadioButton.setDisabled(jqElem, !!newValue);
                        });
                        varstore.clearWatched();
                        break;
                    case scropt.DataBinding.ParsedType.BOOLEAN:
                        RadioButton.setDisabled(jqElem, result.value);
                        var watched = varstore.getWatched();
                        if (watched) {
                            watched.forEach(function (val) {
                                varstore.getModel().observe(val, function (type, target, prop, newValue) {
                                    var newRes = scropt.DataBinding.parseDirect(result.type, attr, varstore);
                                    if (newRes != null) {
                                        RadioButton.setDisabled(jqElem, newRes.value);
                                    }
                                    varstore.clearWatched();
                                });
                            });
                        }
                        break;
                }
            }
            varstore.clearWatched();
        };
        RadioButton.getValue = function (elem) {
            return elem.attr('data-value') || elem.attr('value');
        };
        RadioButton.setDisabled = function (jqElem, disabled) {
            if (disabled) {
                jqElem.attr('disabled', 'disabled');
            }
            else {
                jqElem.removeAttr('disabled');
            }
        };
        RadioButton.selector = "input[type=\"radio\"]:not(.material-clone)";
        return RadioButton;
    }(scropt.Elements.Element));
    Material.RadioButton = RadioButton;
    var RadioButtonGroup = (function (_super) {
        __extends(RadioButtonGroup, _super);
        function RadioButtonGroup() {
            _super.apply(this, arguments);
            this.replace = scropt.Elements.InsertMode.None;
            this.buttons = [];
        }
        RadioButtonGroup.prototype.render = function (group) {
            group.find('input[type="radio"]:not(.material-clone)').addBack('input[type="radio"]:not(.material-clone)').each(function () {
                var elem = $(this);
                var rb = new RadioButton();
                rb.setModel(this.binding);
                rb.renderInternal(elem);
                var fnClk = function (e) {
                    if (elem.attr('disabled')) {
                        return;
                    }
                    group.find('.material-radio[checked]').removeAttr('checked');
                    $(elem).attr('checked', 'checked');
                    var fnChange = $(group).data('change');
                    if (fnChange) {
                        fnChange.apply(group, e);
                    }
                };
                if ($(this).attr('tabindex') === undefined) {
                    $(this).attr('tabindex', 0);
                }
                $(this).off('click').click(function (e) {
                    $(this).removeAttr('data-tabbed');
                    fnClk(e);
                });
                $(this).off('keydown').keydown(function (event) {
                    var code = event.which || event.keyCode;
                    switch (code) {
                        case 13:
                        case 32:
                            fnClk(event);
                            break;
                    }
                });
                $(this).keyup(function (event) {
                    var code = event.which || event.keyCode;
                    if (code === 9) {
                        $(this).attr('data-tabbed', 'true');
                    }
                });
                $(this).next('label').off('click').click(function () {
                    elem.click();
                });
            });
            return undefined;
        };
        RadioButtonGroup.prototype.addRadioButton = function (elem) {
            this.buttons.push(elem);
        };
        RadioButtonGroup.selector = ".material-radio-group:not(.material-clone)";
        return RadioButtonGroup;
    }(scropt.Elements.Element));
    Material.RadioButtonGroup = RadioButtonGroup;
    var DropdownList = (function (_super) {
        __extends(DropdownList, _super);
        function DropdownList() {
            _super.apply(this, arguments);
            this.replace = scropt.Elements.InsertMode.Replace;
        }
        DropdownList.prototype.render = function (match) {
            var elem = this.replaceSelectWithDropdown(match);
            var dropDown = $(elem).find('.material-input-dropdown-menu').addClass('material-input-dropdown-value');
            scropt.Elements.registerObserver(elem, function () {
                dropDown.each(function () {
                    var maxWidth = $(this).width();
                    //console.log('maxWidth', maxWidth);
                    $(this).parent().css({ 'min-width': maxWidth, 'width': maxWidth });
                    $(this).removeClass('material-input-dropdown-value');
                });
            });
            this.initDropdown(elem);
            this.handleScropt(elem);
            return elem;
        };
        DropdownList.prototype.handleScropt = function (elem) {
            var varstore = new scropt.VarStoreImpl(this.binding);
            var attrs = scropt.DataBinding.getScroptAttributes(elem);
            for (var attr in attrs) {
                switch (attr) {
                    case 'model':
                        this.handleModel(attrs[attr], elem);
                        break;
                    case 'disabled':
                        // TODO this.handleDisabled(attrs[attr], elem, varstore);
                        break;
                }
            }
            elem.addClass(scropt.SC_CLASS_UPGRADED);
        };
        DropdownList.prototype.handleModel = function (path, jqElem) {
            var model = this.binding;
            var defValue = model.get(path);
            var defTitle = DropdownList.findTitle(jqElem, defValue);
            scropt.Elements.registerObserver(jqElem, function () {
                if (defTitle !== undefined) {
                    DropdownList.setValue(jqElem, defValue, defTitle);
                }
                else {
                    DropdownList.setValue(jqElem, '', ' ');
                }
            });
            model.observe(path, function (type, target, prop, newValue) {
                var newTitle = DropdownList.findTitle(jqElem, newValue);
                if (newTitle !== undefined) {
                    DropdownList.setValue(jqElem, newValue, newTitle);
                }
                else {
                    DropdownList.setValue(jqElem, '', ' ');
                }
            });
            jqElem.change(function (e) {
                //console.log('change', e);
                if (!e || e.originalEvent) {
                    model.update(path, DropdownList.getValue(jqElem));
                }
            });
        };
        DropdownList.getValue = function (elem) {
            return elem.find('.material-input-dropdown-value').attr('data-value');
        };
        DropdownList.setValue = function (elem, value, title) {
            elem.find('.material-input-dropdown-value').attr('data-value', value).text(title);
        };
        DropdownList.findTitle = function (elem, value) {
            var items = elem.find('.material-input-dropdown-menu > div').toArray();
            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
                var item = items_1[_i];
                var itemr = $(item);
                if (itemr.attr('data-value') == value) {
                    return itemr.children().first().text();
                }
            }
            return undefined;
        };
        DropdownList.prototype.replaceSelectWithDropdown = function (selectElem) {
            var items = [];
            var selected;
            $(selectElem).children().each(function () {
                items.push({ 'text': $(this).text(), 'value': $(this).val() });
                if ($(this).prop('selected')) {
                    selected = items[items.length - 1];
                }
            });
            var dropdown = $('<span class="material-input-dropdown" tabindex="0" />');
            dropdown.off('keydown').keydown(function (event) {
                var code = event.which || event.keyCode;
                switch (code) {
                    case 13:
                    case 32:
                        $(this).click();
                        break;
                }
            });
            //console.log('selected', selected);
            var downValue = $('<span class="material-input-dropdown-value" data-value="' + selected.value + '">' + selected.text + '</span>');
            dropdown.append(downValue);
            dropdown.append($('<i class="material-icons">arrow_drop_down</i>'));
            var valueList = $('<div class="material-input-dropdown-menu unselectable" tabindex="-1">');
            $.each(items, function (i, item) {
                valueList.append($('<div data-value="' + item.value + '" tabindex="1"><span>' + item.text + '</span></div>'));
            });
            dropdown.append(valueList);
            scropt.Elements.copyAttributes(selectElem, dropdown, 'class');
            //$(selectElem).replaceWith(dropdown);
            return dropdown;
        };
        DropdownList.prototype.initDropdown = function (dropdown) {
            var shadowRadius = 5;
            $(dropdown).off('click').click(function (event) {
                var mainCont = $(this);
                var menu = $(this).find('.material-input-dropdown-menu');
                if (menu.hasClass('material-input-dropdown-menu-active')) {
                    $(document).click();
                    return;
                }
                $(document).click();
                event.stopPropagation();
                var valCont = $(this).find('.material-input-dropdown-value');
                var val = $(valCont).attr('data-value');
                scropt.Elements.registerObserver(menu, function () {
                    $(menu).addClass('material-input-dropdown-menu-active');
                });
                $(menu).show();
                var valOuterCont = $(menu).find('div[data-value="' + val + '"]');
                //console.log('valOuterCont', valOuterCont);
                if (valOuterCont.length === 0) {
                    valOuterCont = $(menu).find('div').first();
                }
                //console.log('valOuterCont2', valOuterCont);
                $(menu).scrollTop($(menu).scrollTop() + $(valOuterCont).position().top - ($(menu).height()) / 2 + $(valOuterCont).height());
                //            setTimeout(function () {
                //                $(menu).addClass('material-input-dropdown-menu-active');
                //            }, 10);
                $(window).off('click.dropdown').one('click.dropdown', function () {
                    $(menu).removeClass('material-input-dropdown-menu-active');
                    setTimeout(function () {
                        $(menu).hide();
                    }, 200);
                });
                // fix the $(mainCont).blur() event with this
                var menuMouseDownIndicator = undefined;
                $(menu).off('mousedown').mousedown(function (event) {
                    menuMouseDownIndicator = event;
                }).off('mouseup').mouseup(function () {
                    $(mainCont).focus();
                    menuMouseDownIndicator = undefined;
                });
                $(mainCont).off('blur').blur(function () {
                    if (!menuMouseDownIndicator) {
                        $(document).click();
                    }
                });
                //
                /* OPTIONS */
                $(menu).children().off('keydown').keydown(function (event) {
                    var code = event.which || event.keyCode;
                    event.stopPropagation();
                    switch (code) {
                        case 13:
                        case 32:
                            $(this).click();
                            break;
                    }
                }).off('click').click(function (e) {
                    e.stopPropagation();
                    $(valCont).attr('data-value', $(this).attr('data-value'));
                    $(valCont).text($(this).text());
                    $(menu).removeClass('material-input-dropdown-menu-active');
                    //setTimeout(function(){$(menu).hide();}, 200);
                    var fnChange = $(mainCont).data('change');
                    if (fnChange) {
                        fnChange.apply(mainCont, e);
                    }
                    $(document).click();
                });
                /* moving the whole container to the right position */
                var offMenu = $(menu).offset();
                //var offListItem = $(menu).find('div[data-value="' + val + '"] > span').offset();
                var offListItem = valOuterCont.find('span').offset();
                var offValue = $(this).find('.material-input-dropdown-value').offset();
                //console.log(offMenu);
                //console.log(offListItem);
                //console.log(offValue);
                //console.log($(menu).find('div[data-value="'+val+'"]').position());
                var newTop = offValue.top - offListItem.top + $(this).data('oTop');
                var newLeft = offValue.left - offListItem.left + $(this).data('oLeft');
                var menuTop = offMenu.top + offValue.top - offListItem.top - shadowRadius;
                var menuLeft = offMenu.left + offValue.left - offListItem.left - shadowRadius;
                if (menuTop < 0) {
                    newTop -= menuTop;
                }
                else if (menuTop + $(menu).height() + shadowRadius > $(window).height()) {
                    var diffTop = (menuTop + $(menu).height() + 2 * shadowRadius) - $(window).height();
                    if (menuTop - diffTop >= 0) {
                        newTop -= diffTop;
                    }
                    else {
                        newTop -= menuTop;
                    }
                }
                if (menuLeft < 0) {
                    newLeft -= menuLeft;
                }
                $(this).data('oTop', newTop).data('oLeft', newLeft);
                $(menu).css({ 'top': newTop, 'left': newLeft });
            }).data('oTop', 0).data('oLeft', 0);
        };
        DropdownList.selector = "select:not(.material-clone)";
        return DropdownList;
    }(scropt.Elements.Element));
    Material.DropdownList = DropdownList;
})(Material || (Material = {}));
scropt.Elements.register('FloatingInput', Material.FloatingInput);
scropt.Elements.register('Checkbox', Material.Checkbox);
scropt.Elements.register('RadioButton', Material.RadioButton);
//scropt.Elements.register('RadioButtonGroup', Material.RadioButtonGroup);
scropt.Elements.register('DropdownList', Material.DropdownList);
// proxies for built-in jQuery functions
var proxied_val = $.fn.val;
$.fn.val = function () {
    if (this.hasClass('material-slider-base')) {
        return this.data('val')(arguments);
    }
    else if (this.hasClass('material-input-dropdown')) {
        if (arguments.length === 0) {
            return this.find('.material-input-dropdown-value').attr('data-value');
        }
        else {
            var valToFind = arguments[0];
            var valCont = this.find('.material-input-dropdown-value');
            var newValCont = this.find('.material-input-dropdown-menu > div[data-value="' + valToFind + '"]');
            if (newValCont.length > 0) {
                valCont.attr('data-value', valToFind);
                valCont.text(newValCont.text());
            }
        }
    }
    else if (this.hasClass('material-radio-group')) {
        return this.find('.material-radio[checked]').attr('data-value');
    }
    else if (this.hasClass('material-radio')) {
        return (this.attr('checked')) ? this.attr('data-value') : undefined;
    }
    else if (this.hasClass('material-checkbox-group')) {
        var values = [];
        this.find('.material-checkbox[checked]').each(function () {
            values.push($(this).attr('data-value') || $(this).attr('value'));
        });
        return values;
    }
    else if (this.hasClass('material-checkbox')) {
        return (this.attr('checked')) ? ($(this).attr('data-value') || $(this).attr('value')) : undefined;
    }
    else if (!!$(this).attr('contenteditable')) {
        return this.html();
    }
    else {
        return proxied_val.apply(this, arguments);
    }
};
var proxied_change = $.fn.change;
$.fn.change = function () {
    if (this.hasClass('material-slider-base')) {
        $(this).data('change', arguments[0]);
    }
    else if (this.hasClass('material-input-dropdown')) {
        $(this).data('change', arguments[0]);
    }
    else if (this.hasClass('material-radio-group')) {
        $(this).data('change', arguments[0]);
    }
    else {
        return proxied_change.apply(this, arguments);
    }
};
