/// <reference path="jquery.d.ts" />
/// <reference path="parser.d.ts" />
/// <reference path="sse.d.ts" />
module scropt {

    export const SC_CLASS_UPGRADED = "sc-upgraded";
    export const SC_CLASS_RENDERED = "sc-rendered";
    export const doitElem = "sc-elem";
    export const SC_ATTR_SC_PRE = "sc-";
    export const SC_ATTR_DATA_SC_PRE = "data-sc-";

    export let globals: DataBinding.Model;

    function initMain() {
        InternalNavigation.init();
        DataBinding.init();

        globals = DataBinding.Model.createModel();
    }

    function initUI() {
        $(function () {
            Renderer.render();
        });
    }

    module InnerParser {
        const RULE_ANY = "StartAny";
        const RULE_ARI = "StartAri";
        const RULE_BOOL = "StartBool";
        const RULE_FUNC = "StartFunc";
        const RULE_STRING = "StartString";
        const RULE_VAR_NAME = "StartVarName";
        const RULE_NO_RETURN = "StartNoReturn";
        const RULE_FOR_OF = "StartForOf";

        export enum ParsedType {ANY, ARITHMETIC, BOOLEAN, FUNCTION, STRING, VAR_NAME, NO_RETURN, FOR_OF}

        let PARSER_MAPPING = {}

        PARSER_MAPPING[RULE_ANY] = ParsedType.ANY;
        PARSER_MAPPING[RULE_ARI] = ParsedType.ARITHMETIC;
        PARSER_MAPPING[RULE_BOOL] = ParsedType.BOOLEAN;
        PARSER_MAPPING[RULE_FUNC] = ParsedType.FUNCTION;
        PARSER_MAPPING[RULE_STRING] = ParsedType.STRING;
        PARSER_MAPPING[RULE_VAR_NAME] = ParsedType.VAR_NAME;
        PARSER_MAPPING[RULE_NO_RETURN] = ParsedType.NO_RETURN;
        PARSER_MAPPING[RULE_FOR_OF] = ParsedType.FOR_OF;


        function parseWithRule(raw: string, rule: string, store?: VarStore) {
            try {
                let parsed = scropt.Parser.parse(raw, {startRule: rule, store: store});
                let type = PARSER_MAPPING[rule];
                return {type: type, value: parsed};
                //console.info(this);
                //return scropt.Parser.parse.call(this, raw, {startRule: rule, store: store});
            } catch (e) {
                console.log(e);
                return null;
            }
        }

        export function any(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_ANY, store);
        }

        export function arithmetic(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_ARI, store);
        }

        export function boolean(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_BOOL, store);
        }

        export function func(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_FUNC, store);
        }

        export function str(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_STRING, store);
        }

        export function varName(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_VAR_NAME, store);
        }

        /*export function multi(raw: string, store?: VarStore) {
         return noReturn(raw, store);
         }*/

        export function noReturn(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_NO_RETURN, store);
        }

        export function forOf(raw: string, store?: VarStore) {
            return parseWithRule(raw, RULE_FOR_OF, store);
        }
    }

    function escapeRegExp(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    function resetRegExp(expression: RegExp) {
        expression.lastIndex = 0;
    }

    /*class Navigation {
     private static instance:Navigation;

     constructor() {
     Navigation.instance = new Navigation();
     }

     static isHistorySupported() {
     return !!(window.history && history.pushState);
     }

     private static isHistorySupported2() {
     return !!(window.history && history.pushState);
     }

     static initLinks() {
     $('a:not(.external-link), .internal-link').off('click.internal').on('click.internal', function (event) {
     // TO-DO
     //Navigation.navigate($(this));
     event.preventDefault();
     });
     }

     public static getInstance():Navigation {
     return Navigation.instance;
     }
     }*/

    export module Navigation {
        export function navigate(url: string, external: boolean = false) {
            if (external) {
                // target = _blank maybe?
            } else {
                InternalNavigation.steps++;
                Routing.handleURL(url);
            }
        }

        export function saveState() {

        }

        export function loadState() {

        }
    }

    module InternalNavigation {
        export let steps: number = 0;

        export function init() {
            steps = window.history.length - 1;

            if (isHistorySupported()) {
                window.onpopstate = function (event) {
                    /* TODO */
                    //console.log('onpopstate', event);
                    let state = event.state;
                    steps--;
                    if (state === null) {
                        Navigation.navigate('/', null);
                        //load('', {url: '/page.php?q=main/show'}, null, true);
                    } else {
                        //console.log('pop', state);
                        // globals = state.globalModel;
                        // TODO apply state.pageModel to the pageController
                        //load('', state, null, true);
                        let obj = {
                            model: state.model
                        }
                        Routing.handleURL(state.url, obj);
                    }
                };
            } else {
                console.error("History API is not supported!");
                alert("Your browser is not supported yet :(");
            }
        }

        export function navigationFinished(obj, noHistory: boolean = false) {
            //console.log('navigationFinished', 'obj:', obj, 'noHistory:', noHistory, 'history:', window.history);
            if (InternalRouting.currentPageController) {
                //window.history.replaceState()
                let state = window.history.state;

                //console.log('state', state);

                if (!noHistory) {
                    if (InternalRouting.currentPageController.model && InternalRouting.currentPageController.model.base) {
                        state.model = $.extend(true, {}, InternalRouting.currentPageController.model.base);
                    }
                } else {
                    state.model = $.extend(true, {}, state.model);
                }

                //console.log('replaceState', state);
                window.history.replaceState(state, undefined, state.url);
            }

            InternalRouting.currentPageController = obj.controller;
            InternalRouting.currentPageController.model = obj.model;


            if (isHistorySupported() && !noHistory) {
                let state = {
                    // globalModel: globals.base, // TODO maybe we shouldn't save this because the user will save page data to the pageController's model, not the globals
                    url: obj.path
                    //, url: the current URL, but not the passed url argument
                };

                //console.log('push', state);
                window.history.pushState(state, undefined, obj.path);
            }
        }

        export function isHistorySupported() {
            return !!(window.history && history.pushState);
        }

        export function initLinks() {
            $('a:not(.do-external-link), .do-internal-link').off('click.do-internal').on('click.do-internal', function (event) {
                let url = $(this).attr('href') || $(this).attr('data-link');
                if (!url) {
                    // TODO what should we do?
                }

                Navigation.navigate(url);
                event.preventDefault();
            });
        }

        init();
    }

    interface VarStore {
        get(obj: any);
        set(obj: any, value: any);
    }

    export class VarStoreImpl implements VarStore {
        private model: DataBinding.Model;

        /**
         * Contains the variables that should be watched for changes.
         * @type {Array}
         */
        private watched = [];

        constructor(model: DataBinding.Model) {
            this.model = model;
        }

        public getWatched() {
            return this.watched;
        }

        public clearWatched() {
            this.watched = [];
        }

        public getModel(): DataBinding.Model {
            return this.model;
        }

        public get(obj: any) {
            if (this.watched.indexOf(obj.raw) < 0) {
                this.watched.push(obj.raw);
            }

            var sub = obj;
            var n = sub.name;
            var idx = sub.idx;

            var mod = this.model.value[n];

            if (idx) {
                for (var i = 0; i < idx.length; i++) {
                    n = idx[i];
                    mod = mod[n];
                }
            }

            while (sub = sub.sub) {
                n = sub.name;
                mod = mod[n];
                idx = sub.idx;

                if (idx) {
                    for (var i = 0; i < idx.length; i++) {
                        n = idx[i];
                        mod = mod[n];
                    }
                }
            }

            return mod;
        }

        public set(obj: any, value: any) {
            var n = obj.name;
            var sub = obj;
            var idx = sub.idx;

            var lastMod = this.model.value;

            var mod = lastMod[n];

            if (idx) {
                for (var i = 0; i < idx.length; i++) {
                    lastMod = mod;
                    n = idx[i];
                    mod = mod[n];
                }
            }

            while (sub = sub.sub) {
                lastMod = mod;
                n = sub.name;
                mod = mod[n];
                idx = sub.idx;

                if (idx) {
                    for (var i = 0; i < idx.length; i++) {
                        lastMod = mod;
                        n = idx[i];
                        mod = mod[n];
                    }
                }
            }

            lastMod[n] = value;
        }

        /*public get(name:string) {
         return this.vars[name];
         }

         public set(name:string, value:any) {
         this.vars[name] = value;
         }

         public remove(name:string) {
         delete this.vars[name];
         }

         public clear() {
         this.vars = {};
         }*/
    }

    /**
     * Data binding
     */
    export module DataBinding {
        export var ParsedType = InnerParser.ParsedType;

        var attributes = {
            // model / values
            model: [InnerParser.varName],
            value: [InnerParser.varName, InnerParser.boolean, InnerParser.arithmetic],
            for: [InnerParser.forOf],

            // event functions
            click: [InnerParser.func, InnerParser.noReturn],
            change: [InnerParser.func, InnerParser.noReturn],
            focus: [InnerParser.func, InnerParser.noReturn],
            blur: [InnerParser.func, InnerParser.noReturn],
            keyup: [InnerParser.func, InnerParser.noReturn],
            keydown: [InnerParser.func, InnerParser.noReturn],
            keypress: [InnerParser.func, InnerParser.noReturn],

            // additional
            show: [InnerParser.boolean],
            hide: [InnerParser.boolean],
            disabled: [InnerParser.boolean]
        };

        class ProxyImplWithObserver {
            private target: any;
            private handler: any;

            private observerFn: Function;

            constructor(target: any, handler: any) {
                this.target = target;
                this.handler = handler;

                if (Array.isArray(target)) {
                    this.arrayObserver(this);
                } else if (typeof target == "object") {
                    this.objectObserver(this);
                } else {
                    // TODO what happens?
                }

                return target;
            }

            private objectObserver(_this: ProxyImplWithObserver) {
                var observe = Object["observe"];

                observe(this.target, this.observerFn = function (changes) {
                    changes.forEach(function (change) {
                        var property = change.name;
                        var receiver = change.object;

                        switch (change.type) {
                            case "add":
                                _this.fnSet(property, receiver);
                                break;
                            case "update":
                                _this.fnSet(property, receiver);
                                break;
                            case "delete":
                                _this.fnDelete(property);
                                break;
                            // we don't care about the rest of the cases for now
                        }
                    });
                });
            }

            private arrayObserver(_this: ProxyImplWithObserver) {
                var observe = Array["observe"];

                observe(this.target, this.observerFn = function (changes) {
                    changes.forEach(function (change) {
                        var property = change.name;
                        var receiver = change.object;

                        switch (change.type) {
                            case "add":
                                _this.fnSet(property, receiver);
                                break;
                            case "update":
                                _this.fnSet(property, receiver);
                                break;
                            case "delete":
                                _this.fnDelete(property);
                                break;
                            case "splice":
                                _this.fnSplice(receiver, change.index, change.removed, change.addedCount);
                                break;
                        }
                    });
                });
            }

            private fnSet(property, receiver) {
                if (this.handler.set) {
                    if (receiver.hasOwnProperty(property)) {
                        var value = receiver[property];

                        if (!(value instanceof ProxyImplWithObserver)) {
                            this.handler.set(this.target, property, value, receiver, true);// 5th param 'true' is not official, it's an indicator for the custom proxy handler that this is coming from an observer
                        }
                    }
                }
            }

            private fnSplice(receiver, index, removed, addedCount) {
                for (var i = 0; i < removed.length; i++) {
                    this.fnDelete(index + i);
                }

                for (var i = 0; i < addedCount; i++) {
                    this.fnSet(index + i, receiver);
                }
            }

            private fnDelete(property) {
                if (this.handler.deleteProperty) {
                    this.handler.deleteProperty(this.target, property, true); // 3rd param 'true' is not official, it's an indicator for the custom proxy handler that this is coming from an observer
                }
            }
        }

        var Proxy = undefined;

        export function init() {
            Proxy = window["Proxy"];
            if (Proxy == undefined) {
                if (Object["observe"] != undefined) {
                    var observe = Object["observe"];
                    Proxy = window["Proxy"] = ProxyImplWithObserver;
                } else {
                    // FIXME should do something, e.g. use requestAnimationFrame() and do dirty checking
                    alert("This browser is not supported yet, I'm sorry :(");
                    return;
                }
            }
        }

        export function parse(type: string, value: string, varstore: VarStoreImpl) {
            let parsers = attributes[type];

            if (parsers && Array.isArray(parsers)) {
                for (let parser of parsers) {
                    let result = parser(value, varstore);

                    if (result !== null) {
                        return result;
                    }
                }
            }

            return null;
        }

        export function parseDirect(type: InnerParser.ParsedType, value: string, varstore: VarStoreImpl) {
            switch (type) {
                case ParsedType.BOOLEAN:
                    return InnerParser.boolean(value, varstore);
            }
        }

        // model observer methods

        /*export function createModel(obj: any) {
         if (!Array.isArray(obj) && typeof obj != "object") {
         return obj;
         }

         for (var p in obj) {
         if (obj.hasOwnProperty(p)) {
         obj[p] = createModel(obj[p]);
         }
         }

         return new Proxy(obj, {
         set: function (target, property, value, receiver) {
         if (arguments.length <= 4) { // only set the value if it wasn't a ProxyImplWithObserver
         target[property] = createModel(value);
         } else {
         if (Array.isArray(value) || typeof value == "object") {
         target[property] = createModel(value);
         }
         }

         console.log(property, "value set:", value);
         return true;
         },
         deleteProperty: function (target, property) {
         if (arguments.length <= 2) { // only delete the value if it wasn't a ProxyImplWithObserver
         delete target[property];
         }

         console.log(property, "deleted");
         return true;
         }
         });
         }*/

        export interface ModelObserver {
            onModelUpdate: (target: Object, property: string, newValue: any) => boolean|void,
            onModelDelete: (target: Object, property: string) => boolean|void
        }


        export enum ChangeType {Update, Delete}
        export type SimpleModelObserver = (type: ChangeType, target: Object, prop: string, value?: any) => (boolean|void);

        export function isSimpleObserver(fn: ModelObserver | SimpleModelObserver): fn is SimpleModelObserver {
            return typeof fn === typeof Function;
        }


        export class Model {
            /**
             * It shows whether it exists because a value was set or just because of an observer.
             */
            private real: boolean;

            public base: any;

            public value: any;
            private observers: ModelObserver[] = [];
            public children: {} = {};

            private parent: Model;

            constructor(value: any, parent: Model = undefined, real: boolean = true) {
                this.parent = parent;
                this.value = value;
                this.real = real;
            }

            /*public notify() {
             for (var p in this.observers) {
             if (this.observers.hasOwnProperty(p)) {
             //this.observers[p]();
             }
             }
             }*/

            private notifyUpdate(target: Object, property: string, newValue: any) {
                var stopPropagation = false;

                for (var p in this.observers) {
                    if (this.observers.hasOwnProperty(p)) {
                        let ret = this.observers[p].onModelUpdate(target, property, newValue);

                        if (ret) {
                            stopPropagation = true;
                        }
                    }
                }

                if (this.parent && !stopPropagation) {
                    this.parent.notifyUpdate(target, property, newValue);
                }
            }

            private notifyDelete(target: Object, property: string) {
                //var stopPropagation = false;

                for (var p in this.observers) {
                    if (this.observers.hasOwnProperty(p)) {
                        let ret = this.observers[p].onModelDelete(target, property);
                        /*if (this.observers[p].onModelDelete()) {
                         stopPropagation = true;
                         }*/
                    }
                }

                /*if (this.parent && !stopPropagation) {
                 this.parent.notifyDelete();
                 }*/
            }

            public get(path: string) {
                let varNameObj = InnerParser.varName(path).value;

                let n = varNameObj.name;
                let sub = varNameObj;
                let idx = sub.idx;

                let lastObj;
                let obj = this.value;

                while (sub = sub.sub) {
                    lastObj = obj;
                    obj = obj[n];

                    if (Array.isArray(idx) && idx.length > 0) {
                        while (idx.length > 1) {
                            let id = idx.shift();
                            obj = obj[id];
                        }

                        obj = obj[idx[0]];
                    }

                    n = sub.name;
                    idx = sub.idx;

                    /*if (!sub.sub) {
                     obj = lastObj[n];
                     }*/
                }

                if (Array.isArray(idx) && idx.length > 0) {
                    obj = obj[n];
                    while (idx.length > 1) {
                        let id = idx.shift();
                        obj = obj[id];
                    }

                    return obj[idx[0]];
                } else {
                    return obj[n];
                }
            }

            public update(path: string, value: any) {
                let varNameObj = InnerParser.varName(path).value;

                let n = varNameObj.name;
                let sub = varNameObj;
                let idx = sub.idx;

                let lastObj;
                let obj = this.value;

                while (sub = sub.sub) {
                    lastObj = obj;
                    obj = obj[n];

                    if (Array.isArray(idx) && idx.length > 0) {
                        while (idx.length > 1) {
                            let id = idx.shift();
                            obj = obj[id];
                        }

                        obj = obj[idx[0]];
                    }

                    n = sub.name;
                    idx = sub.idx;

                    /*if (!sub.sub) {
                     obj = lastObj[n];
                     }*/
                }

                if (Array.isArray(idx) && idx.length > 0) {
                    obj = obj[n];
                    while (idx.length > 1) {
                        let id = idx.shift();
                        obj = obj[id];
                    }

                    obj[idx[0]] = value;
                } else {
                    obj[n] = value;
                }
            }

            public delete(path: string) {
                let varNameObj = InnerParser.varName(path).value;

                let n = varNameObj.name;
                let sub = varNameObj;
                let idx = sub.idx;

                let lastObj;
                let obj = this.value;

                while (sub = sub.sub) {
                    lastObj = obj;
                    obj = obj[n];

                    if (Array.isArray(idx) && idx.length > 0) {
                        while (idx.length > 1) {
                            let id = idx.shift();
                            obj = obj[id];
                        }

                        obj = obj[idx[0]];
                    }

                    n = sub.name;
                    idx = sub.idx;

                    /*if (!sub.sub) {
                     obj = lastObj[n];
                     }*/
                }

                if (Array.isArray(idx) && idx.length > 0) {
                    obj = obj[n];
                    while (idx.length > 1) {
                        let id = idx.shift();
                        obj = obj[id];
                    }

                    delete obj[idx[0]];
                } else {
                    delete obj[n];
                }
            }

            public observe(path: string, fn: ModelObserver | SimpleModelObserver) {
                var obj = InnerParser.varName(path).value;

                //console.log('observing', obj);

                var n = obj.name;
                var sub = obj;
                var idx = sub.idx;

                var lastModel: Model;
                var mod: Model = this.children[n];

                if (!mod) {
                    let val = /*(sub.sub !== undefined) ? {} :*/ undefined;

                    mod = this.children[n] = new Model(val, this);
                    /*if(val != undefined){
                     Model.create(mod);
                     }*/
                }

                /*if (idx) {
                 for (var i = 0; i < idx.length; i++) {
                 n = idx[i];
                 if (mod.children[n]) {
                 mod = mod.children[n] = new Model(undefined, mod);
                 }
                 }
                 }*/

                while (sub = sub.sub) {
                    n = sub.name;
                    lastModel = mod;
                    mod = mod.children[n];
                    idx = sub.idx; // FIXME it's unused now

                    let val = /*(sub.sub !== undefined) ? {} :*/ undefined;

                    if (!mod) {
                        mod = lastModel.children[n] = new Model(val, lastModel);

                        /*if(val != undefined){
                         Model.create(mod);
                         }*/
                    }

                    /*if (idx) {
                     for (var i = 0; i < idx.length; i++) {
                     n = idx[i];
                     mod = mod[n];
                     }
                     }*/
                }

                if (isSimpleObserver(fn)) {
                    mod.observe_OLD({
                        onModelUpdate: (target, property, newValue) => {
                            return fn(ChangeType.Update, target, property, newValue);
                        },
                        onModelDelete: (target, property) => {
                            return fn(ChangeType.Delete, target, property);
                        }
                    });
                } else {
                    mod.observe_OLD(fn);
                }

                //lastMod[n] = value;
            }

            public observe_OLD(fn: ModelObserver) {
                this.observers.push(fn);
            }

            public unobserve_OLD(fn: ModelObserver) {
                for (var i = 0; i < this.observers.length; i++) {
                    if (this.observers[i] == fn) {
                        this.observers.splice(i, 1);
                        break;
                    }
                }
            }

            public static createModel(base: Object = {}) {
                //var base = {};
                var m = new Model(base);
                m.base = base;
                this.create(m);
                return m;
            }

            /**
             * Deletes observers in the given model and all of its children.
             * @param model
             */
            private static deleteObservers(property: string, model: Model | undefined) {
                //console.log('deleteObservers', model);
                if (model === undefined) {
                    return;
                }

                var obj = model.value;

                $.each(model.observers, function (i, item) {
                    item.onModelDelete(obj, property);
                });

                //console.log('obj: ', obj);
                for (var p in model.children) {
                    //console.log('prop: ', p);
                    if (model.children.hasOwnProperty(p)) {
                        let child = model.children[p];
                        if (child !== undefined) {
                            Model.deleteObservers(p, child);
                        }
                    }
                }
            }

            private static isArrayIndex(val: any) {
                return !isNaN(val) && 0 === val % 1 && val >= 0;
            }

            private static create(model: Model) {
                var obj = model.value;

                if (!Array.isArray(obj) && typeof obj != "object") {
                    return obj;
                }

                for (var p in obj) {
                    if (obj.hasOwnProperty(p)) {
                        var propModel = new Model(obj[p], model);
                        // TODO test for removing observers if a children gets overwritten
                        // FIXME might be better to merge the new and old children if possible
                        Model.deleteObservers(p, model.children[p]);
                        model.children[p] = propModel;

                        obj[p] = Model.create(propModel);
                    }
                }

                return model.value = new Proxy(obj, {
                    set: function (target, property, value, receiver) {
                        if (arguments.length <= 4) { // only set the value if it wasn't a ProxyImplWithObserver
                            var propModel: Model;

                            var child = model.children[property];

                            //console.log('child', child, target);

                            if (child) {
                                model.children[property].value = value;
                                propModel = model.children[property];
                            } else {
                                model.children[property] = propModel = new Model(value, model);
                            }
                            //propModel.observers = (model.children[property]) ? model.children[property].observers : []; // TODO

                            //model.children[property] = propModel;

                            target[property] = Model.create(propModel);
                        } else {
                            // THIS WAS HERE EARLIER, BUT I DON'T REMEMBER WHY...
                            // if (Array.isArray(value) || typeof value == "object") {
                            var propModel: Model;
                            if (model.children[property]) {
                                model.children[property].value = value;
                                propModel = model.children[property];
                            } else {
                                model.children[property] = propModel = new Model(value, model);
                            }

                            //model.children[property] = propModel;

                            target[property] = Model.create(propModel);
                            //}
                        }

                        if (Array.isArray(target) && Model.isArrayIndex(property)) {
                            model.notifyUpdate(target, property, value); // TODO check this
                        } else if (model.children[property]) {
                            model.children[property].notifyUpdate(target, property, value);
                        }

                        //console.debug("'" + property + "'", "value set:", value);
                        return true;
                    },
                    deleteProperty: function (target, property) {
                        if (arguments.length <= 2) { // only delete the value if it wasn't a ProxyImplWithObserver
                            delete target[property];
                        }

                        if (Array.isArray(target) && Model.isArrayIndex(property)) {
                            model.notifyDelete(target, property);
                        } else if (model.children[property]) {
                            model.children[property].notifyDelete();
                        }
                        delete model.children[property];

                        //console.debug(property, "deleted");
                        return true;
                    }
                });
            }
        }

        // Element methods
        export function getScroptAttributes(elem: JQuery | string) {
            let attrs = {};

            $.each(attributes, function (attr, fns) {
                var value = $(elem).attr(SC_ATTR_SC_PRE + attr) || $(elem).attr(SC_ATTR_DATA_SC_PRE + attr);

                if (value !== undefined) {
                    attrs[attr] = value;
                }
            });

            return attrs;
        }

        export function getAttributes_OLD(elem: Elements.Element | string) {
            var realElem: any;
            if (typeof elem == "string") {
                // classic selector
                realElem = $(elem);
            } else {
                realElem = $((<Elements.Element>elem).selector);
                // object implementing Element
            }
        }

        /**
         * Finds all elements that needs to be processed by scropt.
         */
        export function findScroptElements(container: JQuery, model?: DataBinding.Model) {
            if (model === undefined || model === null) {
                return;
            }

            let varstore = new VarStoreImpl(model);

            // $('[sc-model]:not(.sc-upgrade)');
            const str = Object.keys(attributes).map((i) => `[${SC_ATTR_SC_PRE}${i}]:not(.${SC_CLASS_UPGRADED}),[${SC_ATTR_DATA_SC_PRE}${i}]:not(.${SC_CLASS_UPGRADED})`).join(',');
            //console.log('findScroptElements', 'str:', str);
            $(container).find(str).addBack(str).each((i, item) => {
                let attrs = getScroptAttributes($(item));
                //console.log('findScroptElements', 'attrs:', attrs, $(item).val);
                for (let attr in attrs) {
                    switch (attr) {
                        case 'model':
                            handleModel(attrs[attr], <HTMLElement>item, varstore);
                            break;
                    }
                }


                //$(item).val('akarmi');
            });
        }

        function handleModel(path: string, elem: HTMLElement, varstore: VarStoreImpl) {
            let jqElem = $(elem);

            let model = varstore.getModel();

            let defValue = model.get(path);

            if (elem instanceof HTMLInputElement) {
                switch (elem.type) {
                    case 'checkbox':
                        jqElem.prop('checked', defValue == jqElem.val());
                        jqElem.trigger('change');

                        model.observe(path, (type: ChangeType, target, prop, newValue) => {
                            jqElem.prop('checked', newValue == jqElem.val());
                            jqElem.trigger('change');
                        });

                        jqElem.on('change', (e) => {
                            if (e.originalEvent) {
                                if (jqElem.prop('checked')) {
                                    model.update(path, jqElem.val());
                                } else {
                                    model.update(path, undefined);
                                }
                            }
                        });
                        break;
                    case 'radio':
                        jqElem.prop('checked', defValue == jqElem.val());
                        jqElem.trigger('change');

                        model.observe(path, (type: ChangeType, target, prop, newValue) => {
                            jqElem.prop('checked', newValue == jqElem.val());
                            jqElem.trigger('change');
                        });

                        jqElem.on('change', (e) => {
                            if (e.originalEvent) {
                                model.update(path, jqElem.val());
                            }
                        });
                        break;
                    default: // text, email, etc.
                        jqElem.val(defValue);
                        jqElem.trigger('input');

                        model.observe(path, (type: ChangeType, target, prop, newValue) => {
                            if (newValue !== jqElem.val()) {
                                jqElem.val(newValue);
                                jqElem.trigger('input');
                            }
                        });

                        jqElem.on('input', (e) => {
                            if (e.originalEvent) {
                                model.update(path, jqElem.val());
                            }
                        });
                        break;
                }
            } else if (elem instanceof HTMLSelectElement || elem instanceof HTMLTextAreaElement) {
                jqElem.val(defValue);
                jqElem.trigger('input');

                model.observe(path, (type: ChangeType, target, prop, newValue) => {
                    if (newValue !== jqElem.val()) {
                        jqElem.val(newValue);
                        jqElem.trigger('input');
                    }
                });

                jqElem.on('input', (e) => {
                    if (e.originalEvent) {
                        model.update(path, jqElem.val());
                    }
                });
            } else /*if (//elem instanceof HTMLDivElement &&
             elem.isContentEditable)*/ {
                jqElem.html(defValue);
                jqElem.trigger('input');

                model.observe(path, (type: ChangeType, target, prop, newValue) => {
                    if (newValue !== jqElem.html()) {
                        jqElem.html(newValue || '');
                        jqElem.trigger('input');
                    }
                });

                jqElem.on('input', (e) => {
                    if (e.originalEvent) {
                        model.update(path, jqElem.html());
                    }
                });
            }
        }

        /*export function processElement(elem: Elements.Element) {
         var jElem = $(elem.selector);

         // TODO nope, this is not good, should look for only one element at a time maybe?
         $(jElem).each(function () {
         $.each(this.attributes, function () {
         // this.attributes is not a plain object, but an array
         // of attribute nodes, which contain both the name and value
         //if (this.specified) {
         console.log(this.name, this.value);
         //}
         });
         });
         }*/

        /*export interface ObservedChange {
         name:string;
         object:Observable;
         type:string;
         oldValue?:any;
         }

         export interface Observable {
         propertyChanged: (change:ObservedChange) => void
         }

         export class ObservableObject implements Observable {
         private hiddenProps:Object = {};

         propertyChanged(change:ObservedChange) {

         }

         public addProperty(name:string, val:any) {
         Object.defineProperty(this, name, {
         value: val,
         enumerable: true,
         get: function () {
         return this.hiddenProps[name];
         },
         set: function () {
         this.hiddenProps[name] = val;
         }
         });
         }

         public static setProperty(prop:any, val:any) {

         }
         }*/
    }

    /**
     * IO module which includes Server-Sent Events and WebSockets
     */
    export module IO {
        abstract class CommonConnector<T extends EventTarget> {
            protected url: string;
            protected listeners: {string: [Function]};
            protected source: T;

            public abstract connect();

            public abstract close();

            constructor(url: string) {
                this.url = url;
            }

            public listen(type: string, callback: Function) {
                if (!this.listeners[type]) {
                    this.listeners[type] = [];
                }

                this.listeners[type].push(callback);

                if (this.listeners[type].length === 1) {
                    this.registerEvent(type);
                }
            }

            public unlisten(callback: Function) {
                for (var p in this.listeners) {
                    if (this.listeners.hasOwnProperty(p)) {
                        var idx = -1;

                        this.listeners[p].forEach(function (handler, i) {
                            if (handler === callback) {
                                idx = i;
                                return false;
                            }
                        });

                        if (idx > -1) {
                            this.listeners[p].splice(idx, 1);
                            this.unregisterEvent(p);
                            break;
                        }
                    }
                }
            }

            public onOpen(callback: Function) {
                this.listen('open', callback);
            }

            public onError(callback: Function) {
                this.listen('error', callback);
            }

            public onMessage(callback: Function) {
                this.listen('message', callback);
            }

            public onClose(callback: Function) {
                this.listen('close', callback);
            }

            protected registerEvent(event) {
                if (this.source) {
                    // FIXME this should check whether the event has benn registered already
                    this.source.addEventListener(event, this.internalReceive, false);
                }
            }

            protected unregisterEvent(event: string) {
                if (this.listeners[event].length > 0) {
                    return;
                }

                delete this.listeners[event];

                if (this.source) {
                    this.source.removeEventListener(event);
                }
            }

            protected internalReceive(event) {
                var handlers;

                if ((handlers = this.listeners[event.type]) !== undefined) {
                    handlers.forEach(function (handler) {
                        handler(event);
                    });
                }
            }
        }

        export class SSE extends CommonConnector<sse.IEventSourceStatic> {
            private external: boolean;

            constructor(url: string, external: boolean = false) {
                super(url);
                this.external = external;
            }

            public connect() {
                if (SSE.isAvailable()) {
                    this.source = (external) ? new EventSource(this.url, {withCredentials: true}) : new EventSource(this.url);

                    for (var p in this.listeners) {
                        if (this.listeners.hasOwnProperty(p)) {
                            this.registerEvent(p);
                        }
                    }
                } else {
                    alert(`Sorry, your browser doesn't support Server-Sent Events yet :(`);
                }
            }

            public close() {
                if (SSE.isAvailable()) {
                    if (this.source) {
                        this.source.close();
                        this.internalReceive({type: 'close'}); // little hack
                        this.source = null;
                    }
                } else {
                    alert(`Sorry, your browser doesn't support Server-Sent Events yet :(`);
                }
            }

            public static isAvailable(): boolean {
                return "EventSource" in window;
            }
        }

        export class WebSockets extends CommonConnector<WebSocket> {
            private protocols?: string | string[];

            constructor(url: string, protocols?: string|string[]) {
                super(url);
                this.protocols = protocols;
            }

            public connect() {
                if (WebSockets.isAvailable()) {
                    this.source = new WebSocket(this.url, this.protocols);

                    for (var p in this.listeners) {
                        if (this.listeners.hasOwnProperty(p)) {
                            this.registerEvent(p);
                        }
                    }
                } else {
                    throw new Error('WebSocket not supported');
                }
            }

            public close(code?: number, reason?: string) {
                if (WebSockets.isAvailable()) {
                    if (this.source) {
                        this.source.close(code, reason);
                        this.internalReceive({type: 'close'});
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    alert(`Sorry, your browser doesn't support Server-Sent Events yet :(`);
                }
            }

            public send(data: string|Object) {
                if (WebSockets.isAvailable()) {
                    if (this.source && this.source.readyState == WebSocket.OPEN) {
                        if (typeof data === typeof Object) {
                            data = JSON.stringify(data);
                        }

                        this.source.send(data);

                        return WebSocket.OPEN;
                    } else {
                        return (this.source) ? this.source.readyState : WebSocket.CLOSED;
                    }
                } else {
                    alert(`Sorry, your browser doesn't support Server-Sent Events yet :(`);
                }
            }

            public getActualProtocol() {
                return this.source.protocol;
            }

            public static isAvailable(): boolean {
                return "WebSocket" in window;
            }
        }
    }

    /**
     * Notification API implementation
     * See more at https://developer.mozilla.org/en-US/docs/Web/API/notification
     */
    export module Notifications {
        var Notification = window["Notification"];

        export function init(callback?: Function) {
            if (isAvailable()) {
                requestPermission(callback);
            }
        }

        function requestPermission(callback?: Function) {
            if (Notification.permission !== 'denied') {
                Notification.requestPermission(callback);
            }
        }

        export function notify(title: string, message: string, icon, onclick, tag) {
            if (!isAvailable()) {
                return false;
            }

            // TODO requestPermission(); might be used to ask for permission before delivering the notification

            if (Notification.permission !== "granted") {
                return false;
            }

            var nTag = tag || "notification";

            var n = new Notification(title, {tag: nTag, body: message, icon: icon});
            if (onclick && $.isFunction(onclick)) {
                n.onclick = onclick;
            }

            return true;
        }

        export function isAvailable() {
            return Notification !== undefined;
        }
    }

    /**
     * Page Visibility API implementation
     * See more at https://developer.mozilla.org/en-US/docs/Web/API/Page_Visibility_API
     */
    export module PageVisibility {
        var hidden, visibilityChange;
        var isPageVisible;
        var listeners = [];

        // running some initializations
        if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
            hidden = "hidden";
            visibilityChange = "visibilitychange";
        } else if (typeof document["mozHidden"] !== undefined) {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
        } else if (typeof document["msHidden"] !== undefined) {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
        } else if (typeof document["webkitHidden"] !== undefined) {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
        }

        var handleVisibilityChange = function () {
            isPageVisible = !document[hidden];

            listeners.forEach(function (handler) {
                handler(isPageVisible);
            });
        };

        if (typeof document.addEventListener !== undefined && typeof document[hidden] !== undefined) {
            document.addEventListener(visibilityChange, handleVisibilityChange, false);
        }
        // end of inits

        export function listen(callback: Function) {
            for (var i = 0; i < listeners.length; i++) {
                if (listeners[i] === callback) {
                    return;
                }
            }

            listeners.push(callback);
        }

        export function unlisten(callback: Function): boolean {
            for (var i = 0; i < listeners.length; i++) {
                if (listeners[i] === callback) {
                    listeners.splice(i, 1);
                    return true;
                }
            }

            return false;
        }

        export function clear() {
            listeners.splice(0, listeners.length);
        }

        export function isVisible() {
            return isPageVisible;
        }
    }

    /**
     * Routing
     */

    module InternalRouting {
        export var currentPageController;
    }

    export module Routing {
        interface PageController {
            before?: (url: string) => boolean
            done?: (response?: any, jqXHR?: JQueryXHR) => void;
            fail?: (response?: any, jqXHR?: JQueryXHR) => void;
            target?: () => string;
            params?: {};
            model?: {};
            template?: string | undefined;
            templateUrl?: string | undefined;
            /**
             * If set to false or undefined, the caching will be disabled.
             */
            cache: {
                timeout: 3600 // seconds
            };
        }

        interface PathController {
            path?: string;
            url?: string;
            settings?: {}
            controller?: string;
        }

        interface Param {
            name: string;
            type: RouteNodeParamType;
        }

        class RouteNode {
            static PARAM_REGEXP = new RegExp("\\{([a-zA-Z0-9]+)(?:\\:((?:int|number|word|any)+))?\\}", "g");
            expression: RegExp;
            parent: RouteNode;
            children: RouteNode[] = [];
            /**
             * Used if * is the end of path.
             */
            terminal?: RouteNode | undefined = undefined;

            params: Param[] = [];
            controller: PathController;

            constructor(expression?: string, parent?: RouteNode) {
                if (expression !== undefined) {
                    var processed = RouteNode.processExpression(expression);
                    this.expression = processed.expression;
                    this.params = processed.params;
                }
                this.parent = parent;
            }

            private static createFromProcessed(data: {params: Param[], expression: RegExp}, parent?: RouteNode): RouteNode {
                var node = new RouteNode(undefined, parent);
                node.expression = data.expression;
                node.params = data.params;
                return node;
            }

            private static processExpression(expression: string): {params: Param[], expression: RegExp} {
                // good: \{([a-zA-Z0-9]+)(?:\:((?:int|number|word|any)+))?\}
                var res;
                var offset: number = 0;
                var newExp: string = "^";
                var params: {name: string, type: RouteNodeParamType}[] = [];

                while ((res = RouteNode.PARAM_REGEXP.exec(expression)) != null) {
                    var match = res[0];
                    var param = {name: res[1], type: RouteNodeParamType.Any};

                    // Updating the new expression
                    newExp += escapeRegExp(expression.substring(offset, res.index));
                    offset = res.index + match.length;

                    // Setting the param's type
                    switch (res[2]) {
                        case "int":
                            // (\\-?[0-9]+)
                            param.type = RouteNodeParamType.Int;
                            newExp += "(\\-?[0-9]+)";
                            break;
                        case "number":
                            // (\-?[0-9]+(?:\.[0-9]*)?|\-?\.[0-9]+)
                            param.type = RouteNodeParamType.Number;
                            newExp += "(\\-?[0-9]+(?:\\.[0-9]*)?|\\-?\\.[0-9]+)";
                            break;
                        case "word":
                            // (\w+)
                            param.type = RouteNodeParamType.Word;
                            newExp += "(\\w+)";
                            break;
                        default:
                            // (.+)
                            newExp += "(.+)";
                            break;
                    }
                    params.push(param);
                }

                newExp += escapeRegExp(expression.substring(offset, expression.length)) + "$";
                return {params: params, expression: new RegExp(newExp, "g")};
            }

            public addChild(expression: string): RouteNode {
                return this.findOrAddChild(expression, true);
            }

            public findChild(expression: string): RouteNode {
                return this.findOrAddChild(expression, false);
            }

            private findOrAddChild(expression: string, createIfNotExists: boolean = false): RouteNode {
                var processed = RouteNode.processExpression(expression);

                for (var i in this.children) {
                    var child = this.children[i];

                    if (child.expression.source == processed.expression.source) {
                        return child;
                    }
                }

                if (createIfNotExists) {
                    var node = RouteNode.createFromProcessed(processed, this);
                    this.children.push(node);
                    return node;
                }

                return null;
            }

            public getDataByPath(path: string): {params: {}, child: RouteNode} {
                for (var i in this.children) {
                    var child = this.children[i];

                    var match;

                    if ((match = child.expression.exec(path)) != null) {
                        var params = {};
                        for (var j = 0; j < child.params.length; j++) {
                            params[child.params[j].name] = match[j + 1];
                        }

                        resetRegExp(child.expression);

                        return {params: params, child: child};
                    }

                    /*if (child.expression.test(path)) {
                     return {params: null, child: child};
                     }*/
                }

                return null;
            }
        }

        /**
         * Configuration module
         */
        export module Config {
            var basePath: string = "/";

            export function base(path: string) {
                if (!path) {
                    path = "/";
                }

                routeConfig.base = path.trim();

                return this;
            }

            function getBasePath(): string {
                return basePath;
            }
        }

        /**
         * Global retrieve / error handlers.s
         */
        export module Global {
            export function before(callback: (url: string, data?: {}) => boolean) {
                globalHandlers.before = callback;
            }

            export function done(callback: (params?: {}, response?: any, jqXHR?: JQueryXHR) => boolean) {
                globalHandlers.done = callback;
            }

            export function fail(callback: (status: number, response?: string, jqXHR?: JQueryXHR) => boolean) {
                globalHandlers.fail = callback;
            }
        }

        enum RouteNodeParamType {Int, Number, Word, Any}

        var globalHandlers: {
            before: (url: string, data?: {})=>boolean,
            done: (params?: {}, response?: any, jqXHR?: JQueryXHR) => boolean,
            fail: (status: number, response?: string, jqXHR?: JQueryXHR) => boolean} = {
            before: undefined,
            done: undefined,
            fail: undefined
        };

        var routeConfig = {
            base: "/"
        };

        var rootNode = new RouteNode();

        var pageControllers = {};

        var isEnabled = false;

        /**
         * PUBLIC methods
         */

        /**
         * Format for parameters:
         * {paramName[:type]}
         *
         * Type can be:
         * - int: integer
         * - number: decimal number
         * - word: alphanumeric word matching the following regexp: [A-Za-z0-9_]
         * - any (default): matches everything except '/'
         *
         * For example:
         *
         * /page/{id:int}
         * /page/{id:number}/edit
         * /search/{query}
         * /page/can_be_like_this{id:number}
         * @param path the URL
         * @param controller
         * @returns {scropt.Routing.route}
         */
        export function route(path: string, controller?: PathController) {
            if (!path) {
                throw new Error("The path must be valid.");
            }

            if (!isEnabled) {
                enable(true);
            }

            var currentNode = rootNode;
            var datas = path.split('/');
            for (let i = 0; i < datas.length; i++) {
                let piece = datas[i];

                if (piece !== undefined && piece !== null) {
                    piece = piece.trim();
                    switch (piece) {
                        case '':
                            break;
                        case '*':
                            let terminal = new RouteNode(undefined, currentNode);
                            currentNode.terminal = terminal;
                            currentNode = terminal;
                            break;
                        default:
                            currentNode = currentNode.addChild(piece);
                    }
                }
            }

            controller.path = path;

            currentNode.controller = controller;
            return this;
        }

        export function otherwise(controller?: PathController) {
            // TODO this is not good because the matcher splits the URL by '/' so if there's a URL like 'one/two/three/', it's not gonna match
            route("/*", controller);
            return this;
        }

        export function redirect(path: string) {
            // TODO
            /*if (InternalNavigation.isHistorySupported()) {
             window.history.replaceState(null, '', path);
             }*/
        }

        export function page(name: string, controller: PageController|((response?: any, isSuccessful?: boolean, jqXHR?: JQueryXHR) => void)) {
            if (pageControllers[name] !== undefined) {
                throw new Error(`'${name}' is already defined as a page controller.`);
            }

            if (typeof controller !== "function") {
                pageControllers[name] = controller;
            } else {
                pageControllers[name] = {};
                pageControllers[name].done = function (response?: any, jqXHR?: JQueryXHR) {
                    (<Function>controller).call(this, response, true, jqXHR);
                };
                pageControllers[name].fail = function (response?: any, jqXHR?: JQueryXHR) {
                    (<Function>controller).call(this, response, false, jqXHR);
                };
            }

            return this;
        }

        export function enable(enable: boolean) {
            isEnabled = enable;

            if (isEnabled) {
                $(function () {
                    // TODO
                    //console.log(window.location);
                    // TODO handleURL(window.location.pathname);
                });
            }
        }

        /**
         * PRIVATE methods
         */

        /**
         *
         * @param path
         * @returns RouteNode null if the given path is not registered; a valid RouteNode otherwise
         */
        function getRouteNode(path: string): {params: {}, target: RouteNode} {
            var currentNode = rootNode;
            var datas = path.split('/');

            var params = {};

            for (let i = 0; i < datas.length; i++) {
                let piece = datas[i];

                piece = piece.trim();
                if (piece !== "") {
                    var nodeData = currentNode.getDataByPath(piece);
                    if (nodeData == null && !currentNode.terminal) {
                        return null;
                    } else if (nodeData == null && currentNode.terminal) {
                        currentNode = currentNode.terminal
                        $.extend(params, {'*': datas.slice(i, datas.length).join('/')});
                        break;
                    }

                    currentNode = nodeData.child;
                    $.extend(params, nodeData.params);
                }
            }

            return {params: params, target: currentNode};
        }

        export function handleURL(path: string, popstate?) {
            var node = getRouteNode(path);

            if (node != null) {
                var pathController = Object.create(node.target.controller);
                var pageController: PageController;

                var model = (popstate && popstate.model) ? DataBinding.Model.createModel(popstate.model) : DataBinding.Model.createModel();

                if (pathController.controller) {
                    pageController = <PageController>Object.create(pageControllers[pathController.controller]);
                    pageController.params = node.params;

                    // setting the model
                    pageController.model = model.value;
                }

                if (pathController.url) {
                    if (globalHandlers.before && globalHandlers.before(pathController.url, node.params) === false) {
                        return;
                    }

                    for (var p in node.params) {
                        pathController.url = pathController.url.replace(new RegExp(`{${p}}`, "g"), node.params[p]);
                    }

                    if (pageController && pageController.before && pageController.before(pathController.url) === false) {
                        return;
                    }

                    var ajaxSettings = {data: node.params, context: pageController};
                    if (pathController.settings) {
                        $.extend(ajaxSettings, pathController.settings);
                    }

                    let promises = [];

                    let urlPromise = new Promise((resolve, reject) => {
                        $.ajax(pathController.url, ajaxSettings).done(function (data, textStatus, jqXHR) {
                            resolve({
                                type: 'url',
                                data: data,
                                xhr: jqXHR
                            });
                        }).fail(function (jqXHR) {
                            reject({
                                type: 'url',
                                xhr: jqXHR
                            });
                        });
                    });

                    promises.push(urlPromise);

                    if (pageController.templateUrl) {
                        promises.push(new Promise((resolve, reject) => {
                            // TODO first we should try to load from cache

                            $.get(`${pageController.templateUrl}?${Date.now()}`) // bypassing caching
                                .done((data, textStatus, jqXHR) => {
                                    resolve({
                                        type: 'template',
                                        data: data,
                                        xhr: jqXHR
                                    });
                                })
                                .fail((jqXHR, textStatus, errorThrown) => {
                                    reject({
                                        type: 'template',
                                        xhr: jqXHR
                                    });
                                });
                        }));
                    }

                    // here we load the normal url and the template, if needed
                    Promise.all(promises)
                        .then(values => {
                            if (globalHandlers.done && globalHandlers.done(node.params, values[0]['data'], values[0]['xhr']) === false) {
                                return;
                            }

                            let obj = {
                                path: path,
                                controller: pageController,
                                model: model
                            }

                            if (values.length === 1) {
                                // url only, but we need to check template anyways
                                if (pageController.template) {
                                    responseWithUrlAndTemplate(obj, values[0], pageController.template, popstate);
                                } else {
                                    responseWithUrlAndTemplate(obj, values[0], null, popstate);
                                }
                            } else {
                                // url + templateURL
                                responseWithUrlAndTemplate(obj, values[0], values[1], popstate);
                            }

                        })
                        .catch(reason => {
                            console.log('ERROR', reason);
                            let jqXHR = reason.xhr;
                            if (globalHandlers.fail && globalHandlers.fail(jqXHR.status, jqXHR.responseText, jqXHR) === false) {
                                return;
                            }

                            pageController.fail(jqXHR.responseText, jqXHR);
                        });

                    /*TODO using this inside a Promise
                     $.ajax(pathController.url, ajaxSettings).done(function (data, textStatus, jqXHR) {
                     if (globalHandlers.done && globalHandlers.done(node.params, data, jqXHR) === false) {
                     return;
                     }

                     if (pageController) {
                     pageController.done(data, jqXHR);
                     }

                     // FIX ME this should be done if before the view is being applied but only if the view is really applied
                     // FIX ME also, this should be in 'fail' too if the user wants to show an error page, that should also have it

                     // FIX ME BETTER IDEA: the pagecontroller (and the model ofc) should be on the history stack
                     InternalRouting.currentPageController = pageController;
                     InternalRouting.currentPageController.model = model;

                     InternalNavigation.navigationFinished(path, pageController);

                     // TO DO apply the loaded page to the main view
                     $('#page-target').html(data);
                     }).fail(function (jqXHR) {
                     if (globalHandlers.fail && globalHandlers.fail(jqXHR.status, jqXHR.responseText, jqXHR) === false) {
                     return;
                     }

                     if (pageController) {
                     pageController.fail(jqXHR.responseText, jqXHR);
                     }
                     });
                     */
                } else {
                    // TODO ???
                }
            } else {
                // TODO no node found, the URL is invalid, 404, etc.
            }
        }

        export function handle(url: string) {
            handleURL(url.trim().replace(new RegExp("^" + routeConfig.base), ""));
        }

        function responseWithUrlAndTemplate(obj, urlData, templateData, popstate?) {
            //console.info('responseWithUrlAndTemplate', arguments);
            if (obj.controller) {
                obj.controller.done(urlData.data, urlData.jqXHR);
            }

            // FIXME this should be done if before the view is being applied but only if the view is really applied
            // FIXME also, this should be in 'fail' too if the user wants to show an error page, that should also have it

            // FIXME BETTER IDEA: the pagecontroller (and the model ofc) should be on the history stack
            //InternalRouting.currentPageController = obj.controller;
            //InternalRouting.currentPageController.model = obj.model;

            InternalNavigation.navigationFinished(obj, !!popstate);


            var view;
            if (templateData != null) {
                view = templateData.data;
                let modelData = (popstate && popstate.model) ? popstate.model : urlData.data;
                $.extend(true, obj.model.value, modelData);
            } else if (urlData != null) {
                view = urlData.data;
            }

            try {
                view = $('<div>' + view + '</div>');
                Renderer.render(view, obj.model);
                view = view.children();
                //console.log(view, 'newView:', newView);
            } catch (error) {
                console.error('render error', error);
            }

            // TODO apply the loaded page to the main view
            $('#page-target').html(view);
        }
    }

    module Renderer {

        /**
         *
         * @param container The elements to be rendered in. Note that this should be a root node, otherwise the top level nodes won't be processed.
         * @param model The model to be used for rendering.
         */
        export function render(container?: JQuery|Element, model?: DataBinding.Model) {
            var realContainer: JQuery;

            if (!container) {
                realContainer = $(document);
            } else {
                realContainer = $(container);
            }

            FrontPartStorage.elements.forEach(function (elemHolder) {
                // TODO [REMOVED FOR TESTING] console.log("holder:", elemHolder, "selector:", elemHolder.element.selector);
                // TODO [REMOVED FOR TESTING] console.log(realContainer, realContainer.find(elemHolder.element.selector));
                let selector = `${elemHolder.element.selector}:not(${SC_CLASS_RENDERED})`;
                realContainer.find(selector)/*.addBack(selector)*/.each(function (_, e) {
                    let elem = $(e);
                    //console.log("elem found:", elem);
                    if (!elem.hasClass(SC_CLASS_RENDERED)) { // needs this because we might have modified in the middle
                        //console.log("elem is not rendered yet", elem);
                        var newElem = elemHolder.init();
                        newElem.setModel(model);
                        var newElemObj = newElem.renderInternal(elem);

                        if (newElem.replace !== Elements.InsertMode.Manual) {
                            elem.addClass(SC_CLASS_RENDERED);
                            if (newElemObj) {
                                newElemObj.addClass(SC_CLASS_RENDERED);
                            }
                        }

                        if (newElemObj) {
                            initHandlers(newElemObj);

                            // TODO [REMOVED FOR TESTING] console.info(elem, newElem, newElemObj);

                            switch (newElem.replace) {
                                case Elements.InsertMode.Insert:
                                    elem.append(newElemObj);
                                    break;
                                case Elements.InsertMode.Replace:
                                    elem.replaceWith(newElemObj);
                                    break;
                                case Elements.InsertMode.ReplaceKeepContents:
                                    elem.wrapInner(newElemObj);
                                    newElemObj.unwrap();
                                    break;
                                case Elements.InsertMode.Wrap:
                                    elem.wrap(newElemObj);
                                    break;
                                case Elements.InsertMode.None:
                                    // do nothing
                                    break;
                            }
                        }
                    }
                });
                console.groupEnd();
            });
            // do some sc-* attribute processing after we have the new elements
            DataBinding.findScroptElements(realContainer, model);
        }

        function initHandlers(elem: JQuery) {

        }
    }
    /**
     * Elements
     */
    export module Elements {
        export enum InsertMode {Insert, Replace, ReplaceKeepContents, Wrap, None, Manual}

        export function register(name: string, element: IElement|any) {
            // TODO [REMOVED FOR TESTING] console.log(name, element, typeof element);
            FrontPartStorage.addElement(name, element);
            /*if (element.hasOwnProperty('render') && typeof element !== typeof Function) {
             // it's a simple object
             FrontPartStorage.addElement(name, Object.create(element));
             } else {
             // it's an Element classname
             //FrontPartStorage.addElement(name, new (<typeof Element>element)());
             //FrontPartStorage.addElement(name, Object.create(element));
             FrontPartStorage.addElement(name, new (<typeof element>element)());
             }*/
            return this;
        }

        export function copyAttributes(src: JQuery | HTMLElement, dst: JQuery | HTMLElement, ...exclude: string[]) {
            let srcAttrs = $(src)[0].attributes;

            const shouldExclude = exclude && exclude.length > 0;

            for (let i = 0; i < srcAttrs.length; i++) {
                let attr = srcAttrs.item(i);
                if (!shouldExclude || exclude.indexOf(attr.name)) {
                    if (attr.name !== "class") {
                        $(dst).attr(attr.name, attr.value);
                    } else {
                        $(dst).addClass(attr.value);
                    }
                }
            }
        }

        export function registerObserver(target, callback) {
            var jTarget = $(target);

            if ("MutationObserver" in window) {
                var config = {attributes: true, childList: true, characterData: true, subtree: true};
                for (let i = 0; i < jTarget.length; i++) {
                    let t = jTarget.get(i);

                    var observer = new MutationObserver(function (mutations) {
                        observer.disconnect();
                        // console.log(mutations);
                        if (callback !== undefined) {
                            callback(mutations);
                        }
                    });
                    observer.observe(t, config);
                }
            } else {
                for (let i = 0; i < jTarget.length; i++) {
                    let t = jTarget.get(i);
                    $(t).one('DOMSubtreeModified propertychange', function () {
                        if (callback !== undefined) {
                            callback({origin: t});
                        }
                    });
                }
            }
        };

        interface IElement {
            name?: string;
            selector: string; // .class, #id, tag-name
            parents?: string|string[]; // .class, #id, tag-name, !elementName
            replace?: InsertMode;
            model: {};
            render: (match: JQuery) => string|JQuery;
        }

        export abstract class Element implements IElement {
            public name: string;
            public selector: string;
            public parents: string|string[];
            public replace: InsertMode = InsertMode.None;

            protected binding: DataBinding.Model;
            // for rendering
            protected parent: Element | JQuery;
            public model = {};

            constructor() {
                // FIXME ezeknek a PageController-ből kell jönnie!!!
                this.binding = DataBinding.Model.createModel();
                this.model = this.binding.value;
            }

            setModel(model?: DataBinding.Model | undefined) {
                //console.log('>>> MODEL: ', model);
                if (model !== undefined) {
                    this.binding = model;
                    this.model = model.value;
                }
            }

            protected findAncestor(match: JQuery): JQuery {
                var selectors = undefined;

                if (this.parents !== undefined) {
                    if (typeof this.parents === "string") {
                        var p = (<string>this.parents).trim();
                        if (p.indexOf("!") === 0) {
                            // find the element by name and set its selector as the value of selectors
                            selectors = FrontPartStorage.elementByName(p.substring(1)).selector;
                        } else {
                            selectors = p;
                        }
                    } else {
                        var pArray = <Array<string>>this.parents;

                        for (var i = 0; i < pArray.length; i++) {
                            if (pArray[i].trim().indexOf("!") === 0) {
                                // find the element by name and set its selector as the value of selectors
                                pArray[i] = FrontPartStorage.elementByName(pArray[i].substring(1)).selector;
                            }
                        }

                        selectors = pArray.join(",");
                    }

                    if (selectors !== undefined) {
                        var elem;
                        var par = $(match).parent();
                        if (par.length > 0) {
                            elem = $(match).closest(selectors, par.get(0));
                        } else {
                            elem = $(match).closest(selectors);
                        }

                        if (elem !== undefined && elem.length === 1) {
                            return <JQuery>elem;
                        }
                    }
                }

                return undefined;
            }

            renderInternal(match: JQuery): JQuery {
                var ancestor = this.findAncestor(match);
                if (ancestor !== undefined) {
                    if (ancestor.hasClass(SC_CLASS_UPGRADED)) {
                        // get the element
                        this.parent = ancestor;//.data(doitElem);
                    } else {
                        // try to instantiate the parent?
                    }
                }

                var renderRaw = this.render(match);
                if (renderRaw) {
                    return $(renderRaw);
                }

                return null;
            }

            public getModel() {
                return this.model;
            }

            public render(match: JQuery): string|JQuery {
                return match;
            };
        }
    }

    /**
     * Services
     */
    export module Services {

        /**
         * Service, a.k.a. singleton
         * @param name the name of the service
         * @param service the service
         */
        export function register(name: string, service: Service|IService|typeof Service) {
            if (service instanceof Service) {
                // it's a Service instance
                FrontPartStorage.addService(name, service);
            } else if (service.hasOwnProperty('init')) {
                // it's a simple object
                FrontPartStorage.addService(name, new ServiceWrapper(<IService>service));
            } else {
                // it's a Service classname
                FrontPartStorage.addService(name, new (<typeof Service>service)());
            }
            //FrontPartStorage.addService(name, service);
            return this;
        }

        export function get<T extends Service>(name: string): T {
            return FrontPartStorage.getService(name, <T>null);
        }

        /**
         * Interface to allow simple objects to be passed as service.
         */
        interface IService {
            /**
             * It will be called the first time the service is accessed.
             */
            init?: () => void
        }

        /**
         * Service
         */
        export class Service {

            public init() {
                // do nothing, the devs can override it
            };
        }

        /**
         * A wrapper class for the interface so it can be stored like a Service instance.
         */
        class ServiceWrapper extends Service {
            constructor(service: IService) {
                super();

                /*for(var key in service) {
                 this[key] = service[key];
                 }*/
                var keys = Object.keys(service);
                let _this = this;

                for (var i = 0; i < keys.length; i++) {
                    this[keys[i]] = service[keys[i]];
                    /*var value = service[keys[i]];
                     if (typeof value === typeof Function) {
                     this[keys[i]] = function () {
                     value.call(_this, arguments);
                     }
                     } else {
                     this[keys[i]] = value;
                     }*/
                }
            }

            /*public init() {
             this.service.init.call(this);
             }*/
        }
    }

    module FrontPartStorage {
        //var elements:Elements.Element[] = [];

        export var elements: ElementHolder[] = [];
        var elementsBySelector = {};
        //export var elements = {};
        var services: ServiceHolder[] = [];

        /**
         * @param name
         * @param elem

         export function addElement(name:string, elem:Elements.Element) {
            console.info(name, elem);
            elements[name] = elem;
        }*/

        export function addElement(name: string, elem: any) {
            if (!elem.hasOwnProperty("name")) {
                elem.name = name;
                elem.getName = function () {
                    return elem.name;
                }
            }
            var holder = new ElementHolder(elem);

            // TODO [REMOVED FOR TESTING] console.debug("selector", elem.selector);

            elementsBySelector[elem.selector] = holder;
            elements.push(holder);
        }

        export function addService(name: string, service: Services.Service) {
            services[name] = new ServiceHolder(service);
        }

        export function elementByName<T extends Elements.Element>(name: string): T {
            for (var i = 0; i < elements.length; i++) {
                var elem = elements[i].element;
                if (elem && elem.name === name) {
                    return <T>(<any>elements[i].init()); // FIXME should we really instantiate it?
                }
            }

            throw new ReferenceError(`There's no element registered with the name '${name}'.`);
        }

        export function elementBySelector<T extends Elements.Element>(selector: string): T {
            var elem = elementsBySelector[selector];
            if (elem === undefined) {
                throw new ReferenceError(`There's no element registered with the selector '${selector}'.`);
            }

            return <T>(elem.init()); // FIXME should we really instantiate it?
        }

        export function getService<T extends Services.Service>(name: string, typeHack: T): T {
            var service = services[name];
            if (service === undefined) {
                throw new ReferenceError(`There's no service registered with the name '${name}'.`);
            }

            return <T>(service.get(<T>null));
        }

        class ElementHolder {
            element: any;

            constructor(element: any) {
                this.element = element;
            }

            public init(): Elements.Element {
                var elem = this.element;
                if (elem.hasOwnProperty('render') && typeof elem !== typeof Function) {
                    // it's a simple object
                    return Object.create(elem);
                } else {
                    // it's a class extending Elements.Element
                    return new (<typeof elem>elem)();
                }
            }
        }

        /**
         * Service holder for holding the service instance and init it in case of first retrieval.
         */
        class ServiceHolder {
            private service: Services.Service;
            private initialized: boolean = false;

            constructor(service: Services.Service) {
                this.service = service;
            }

            public get<T extends Services.Service>(typeHack: T): T {
                if (!this.initialized) {
                    this.initialized = true;
                    //var service = <T>Object.create(this.service);
                    var service = this.service;
                    service.init();
                    return <T>service;
                }
            }
        }
    }

    class A extends scropt.Elements.Element {
        static selector = ".a";
        replace = Elements.InsertMode.Insert;

        public render(match: JQuery): string {
            return `<p>Valami amerika</p>`;
        }
    }

    class InputTest extends scropt.Elements.Element {
        static selector = "input";//[type='text']
        replace = Elements.InsertMode.Wrap;

        render(match: JQuery): string|JQuery {
            let attrs = DataBinding.getScroptAttributes(match);
            //console.log('attrs: ', attrs);
            //console.log('BINDING', this.binding);
            /*if (attrs['model'] !== undefined) {
             //console.log(InnerParser.varName(attrs['model']));

             // FIXME a this.model valójában a PageController-ből kell jöjjön!!!
             let model = this.model;
             //model['data'] = {input: ''};

             $(match).val(model['data']['input']);

             $(match).on('input', function () {
             model['data']['input'] = $(this).val();
             });

             this.binding.observe(attrs['model'], {
             onModelUpdate: () => {
             //console.log('UPDATE on ', attrs['model']);
             if (model['data']['input'] !== $(match).val()) {
             $(match).val(model['data']['input']);
             }
             //if($(match).val());
             },
             onModelDelete: () => {
             //console.log('DELETE on ', attrs['model']);
             }
             });
             }*/

            //match.replaceWith('<span>WOWOW</span>');
            return $(`<div style="border: 1px solid red; padding: 12px;"/>`);
        }
    }

    //Elements.register("a", A);
    //Elements.register("input", InputTest);
    // preparing things for the framework, not working on actual data yet
    initMain();

    // rendering happens later when the page is ready
    initUI();

    $(function () {
        var model = DataBinding.Model.createModel();

        console.log('MODEL PRE', model);


        var x = model.value;
        window['MODEL'] = model;

        //model.value.a = {b: {c: {d: 23}}};

        model.observe('a.b.c', {
            onModelUpdate: () => {
                console.log('update ' + Date.now());
            },
            onModelDelete: () => {
                console.log('deleted a.b.c observer');
            }
        });

        //model.value.a = {b: {c: {d: 23}}};
        model.value.a = {b: {c: 23}};

        //model.value.a.b.c = 111;
        //delete x.a.b.c;

        console.log('MODEL POST', model);

        /*scropt.ElementM.register('a', {
         replace: false,
         selector: '.',
         render: function () {
         return "";
         }
         });*/
        /*Elements.register("A", A);
         //Elements.register("A", new A());
         Elements.register("B", {
         render: function (match:JQuery) {

         }
         });*/

        // testing the data binder
        /* TODO [REMOVED FOR TESTING] removed tests
         var model = DataBinding.Model.createModel();

         var x = model.value;

         //var o = {};
         //var x = DataBinding.createModel(o);
         x.a = "alma";
         x.b = 238;
         x.c = {foo: "bar"};
         x.d = ["beka", "cekla"];
         x.e = {x: {y: 213, z: ["f", "g", "h"], m: {}}};

         window["X"] = x;

         // observation
         window["M"] = model;
         model.observe('a', {
         onModelUpdate: function () {
         console.log("'a' updated");
         },
         onModelDelete: function () {
         console.log("'a' deleted");
         }
         });

         model.observe('d', {
         onModelUpdate: function () {
         console.log("'d' updated");
         },
         onModelDelete: function () {
         console.log("'d' deleted");
         }
         });

         model.observe('e.x.m', {
         onModelUpdate: function () {
         console.log("'e.x.m' updated");
         },
         onModelDelete: function () {
         console.log("'e.x.m' deleted");
         }
         });*/

        // new observation method test
        // FIXME not working
        /*var model = DataBinding.Model.createModel();
         console.log('before');
         model.value.a = 42;
         model.observe("a", {
         onModelUpdate: function () {console.log("'a' updated");},
         onModelDelete: function () {console.log("'a' deleted");}
         });
         model.value.a = 5;
         console.log('after');*/

        // TODO [REMOVED FOR TESTING] grammar - databinding test
        // var store = new VarStoreImpl(model);
        //InnerParser.noReturn("e.x.z[1] = 'nope'", store);
    });
}