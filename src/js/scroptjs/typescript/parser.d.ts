/*interface ParserOptions {
    startRule?:string
    tracer?:any
}
interface Parser {
    parse(raw:string, options?:ParserOptions) : any;
}

declare var Parser : Parser;*/

declare module scropt {
    module Parser {
        interface ParserOptions {
            startRule?:string
            store?:any
            tracer?:any
        }

        export function parse(raw:string, options?:ParserOptions) : any;
    }
}