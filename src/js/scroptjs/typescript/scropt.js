var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="jquery.d.ts" />
/// <reference path="parser.d.ts" />
/// <reference path="sse.d.ts" />
var scropt;
(function (scropt) {
    scropt.SC_CLASS_UPGRADED = "sc-upgraded";
    scropt.SC_CLASS_RENDERED = "sc-rendered";
    scropt.doitElem = "sc-elem";
    scropt.SC_ATTR_SC_PRE = "sc-";
    scropt.SC_ATTR_DATA_SC_PRE = "data-sc-";
    function initMain() {
        InternalNavigation.init();
        DataBinding.init();
        scropt.globals = DataBinding.Model.createModel();
    }
    function initUI() {
        $(function () {
            Renderer.render();
        });
    }
    var InnerParser;
    (function (InnerParser) {
        var RULE_ANY = "StartAny";
        var RULE_ARI = "StartAri";
        var RULE_BOOL = "StartBool";
        var RULE_FUNC = "StartFunc";
        var RULE_STRING = "StartString";
        var RULE_VAR_NAME = "StartVarName";
        var RULE_NO_RETURN = "StartNoReturn";
        var RULE_FOR_OF = "StartForOf";
        (function (ParsedType) {
            ParsedType[ParsedType["ANY"] = 0] = "ANY";
            ParsedType[ParsedType["ARITHMETIC"] = 1] = "ARITHMETIC";
            ParsedType[ParsedType["BOOLEAN"] = 2] = "BOOLEAN";
            ParsedType[ParsedType["FUNCTION"] = 3] = "FUNCTION";
            ParsedType[ParsedType["STRING"] = 4] = "STRING";
            ParsedType[ParsedType["VAR_NAME"] = 5] = "VAR_NAME";
            ParsedType[ParsedType["NO_RETURN"] = 6] = "NO_RETURN";
            ParsedType[ParsedType["FOR_OF"] = 7] = "FOR_OF";
        })(InnerParser.ParsedType || (InnerParser.ParsedType = {}));
        var ParsedType = InnerParser.ParsedType;
        var PARSER_MAPPING = {};
        PARSER_MAPPING[RULE_ANY] = ParsedType.ANY;
        PARSER_MAPPING[RULE_ARI] = ParsedType.ARITHMETIC;
        PARSER_MAPPING[RULE_BOOL] = ParsedType.BOOLEAN;
        PARSER_MAPPING[RULE_FUNC] = ParsedType.FUNCTION;
        PARSER_MAPPING[RULE_STRING] = ParsedType.STRING;
        PARSER_MAPPING[RULE_VAR_NAME] = ParsedType.VAR_NAME;
        PARSER_MAPPING[RULE_NO_RETURN] = ParsedType.NO_RETURN;
        PARSER_MAPPING[RULE_FOR_OF] = ParsedType.FOR_OF;
        function parseWithRule(raw, rule, store) {
            try {
                var parsed = scropt.Parser.parse(raw, { startRule: rule, store: store });
                var type = PARSER_MAPPING[rule];
                return { type: type, value: parsed };
            }
            catch (e) {
                console.log(e);
                return null;
            }
        }
        function any(raw, store) {
            return parseWithRule(raw, RULE_ANY, store);
        }
        InnerParser.any = any;
        function arithmetic(raw, store) {
            return parseWithRule(raw, RULE_ARI, store);
        }
        InnerParser.arithmetic = arithmetic;
        function boolean(raw, store) {
            return parseWithRule(raw, RULE_BOOL, store);
        }
        InnerParser.boolean = boolean;
        function func(raw, store) {
            return parseWithRule(raw, RULE_FUNC, store);
        }
        InnerParser.func = func;
        function str(raw, store) {
            return parseWithRule(raw, RULE_STRING, store);
        }
        InnerParser.str = str;
        function varName(raw, store) {
            return parseWithRule(raw, RULE_VAR_NAME, store);
        }
        InnerParser.varName = varName;
        /*export function multi(raw: string, store?: VarStore) {
         return noReturn(raw, store);
         }*/
        function noReturn(raw, store) {
            return parseWithRule(raw, RULE_NO_RETURN, store);
        }
        InnerParser.noReturn = noReturn;
        function forOf(raw, store) {
            return parseWithRule(raw, RULE_FOR_OF, store);
        }
        InnerParser.forOf = forOf;
    })(InnerParser || (InnerParser = {}));
    function escapeRegExp(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }
    function resetRegExp(expression) {
        expression.lastIndex = 0;
    }
    /*class Navigation {
     private static instance:Navigation;

     constructor() {
     Navigation.instance = new Navigation();
     }

     static isHistorySupported() {
     return !!(window.history && history.pushState);
     }

     private static isHistorySupported2() {
     return !!(window.history && history.pushState);
     }

     static initLinks() {
     $('a:not(.external-link), .internal-link').off('click.internal').on('click.internal', function (event) {
     // TO-DO
     //Navigation.navigate($(this));
     event.preventDefault();
     });
     }

     public static getInstance():Navigation {
     return Navigation.instance;
     }
     }*/
    var Navigation;
    (function (Navigation) {
        function navigate(url, external) {
            if (external === void 0) { external = false; }
            if (external) {
            }
            else {
                InternalNavigation.steps++;
                Routing.handleURL(url);
            }
        }
        Navigation.navigate = navigate;
        function saveState() {
        }
        Navigation.saveState = saveState;
        function loadState() {
        }
        Navigation.loadState = loadState;
    })(Navigation = scropt.Navigation || (scropt.Navigation = {}));
    var InternalNavigation;
    (function (InternalNavigation) {
        InternalNavigation.steps = 0;
        function init() {
            InternalNavigation.steps = window.history.length - 1;
            if (isHistorySupported()) {
                window.onpopstate = function (event) {
                    /* TODO */
                    //console.log('onpopstate', event);
                    var state = event.state;
                    InternalNavigation.steps--;
                    if (state === null) {
                        Navigation.navigate('/', null);
                    }
                    else {
                        //console.log('pop', state);
                        // globals = state.globalModel;
                        // TODO apply state.pageModel to the pageController
                        //load('', state, null, true);
                        var obj = {
                            model: state.model
                        };
                        Routing.handleURL(state.url, obj);
                    }
                };
            }
            else {
                console.error("History API is not supported!");
                alert("Your browser is not supported yet :(");
            }
        }
        InternalNavigation.init = init;
        function navigationFinished(obj, noHistory) {
            if (noHistory === void 0) { noHistory = false; }
            //console.log('navigationFinished', 'obj:', obj, 'noHistory:', noHistory, 'history:', window.history);
            if (InternalRouting.currentPageController) {
                //window.history.replaceState()
                var state = window.history.state;
                //console.log('state', state);
                if (!noHistory) {
                    if (InternalRouting.currentPageController.model && InternalRouting.currentPageController.model.base) {
                        state.model = $.extend(true, {}, InternalRouting.currentPageController.model.base);
                    }
                }
                else {
                    state.model = $.extend(true, {}, state.model);
                }
                //console.log('replaceState', state);
                window.history.replaceState(state, undefined, state.url);
            }
            InternalRouting.currentPageController = obj.controller;
            InternalRouting.currentPageController.model = obj.model;
            if (isHistorySupported() && !noHistory) {
                var state = {
                    // globalModel: globals.base, // TODO maybe we shouldn't save this because the user will save page data to the pageController's model, not the globals
                    url: obj.path
                };
                //console.log('push', state);
                window.history.pushState(state, undefined, obj.path);
            }
        }
        InternalNavigation.navigationFinished = navigationFinished;
        function isHistorySupported() {
            return !!(window.history && history.pushState);
        }
        InternalNavigation.isHistorySupported = isHistorySupported;
        function initLinks() {
            $('a:not(.do-external-link), .do-internal-link').off('click.do-internal').on('click.do-internal', function (event) {
                var url = $(this).attr('href') || $(this).attr('data-link');
                if (!url) {
                }
                Navigation.navigate(url);
                event.preventDefault();
            });
        }
        InternalNavigation.initLinks = initLinks;
        init();
    })(InternalNavigation || (InternalNavigation = {}));
    var VarStoreImpl = (function () {
        function VarStoreImpl(model) {
            /**
             * Contains the variables that should be watched for changes.
             * @type {Array}
             */
            this.watched = [];
            this.model = model;
        }
        VarStoreImpl.prototype.getWatched = function () {
            return this.watched;
        };
        VarStoreImpl.prototype.clearWatched = function () {
            this.watched = [];
        };
        VarStoreImpl.prototype.getModel = function () {
            return this.model;
        };
        VarStoreImpl.prototype.get = function (obj) {
            if (this.watched.indexOf(obj.raw) < 0) {
                this.watched.push(obj.raw);
            }
            var sub = obj;
            var n = sub.name;
            var idx = sub.idx;
            var mod = this.model.value[n];
            if (idx) {
                for (var i = 0; i < idx.length; i++) {
                    n = idx[i];
                    mod = mod[n];
                }
            }
            while (sub = sub.sub) {
                n = sub.name;
                mod = mod[n];
                idx = sub.idx;
                if (idx) {
                    for (var i = 0; i < idx.length; i++) {
                        n = idx[i];
                        mod = mod[n];
                    }
                }
            }
            return mod;
        };
        VarStoreImpl.prototype.set = function (obj, value) {
            var n = obj.name;
            var sub = obj;
            var idx = sub.idx;
            var lastMod = this.model.value;
            var mod = lastMod[n];
            if (idx) {
                for (var i = 0; i < idx.length; i++) {
                    lastMod = mod;
                    n = idx[i];
                    mod = mod[n];
                }
            }
            while (sub = sub.sub) {
                lastMod = mod;
                n = sub.name;
                mod = mod[n];
                idx = sub.idx;
                if (idx) {
                    for (var i = 0; i < idx.length; i++) {
                        lastMod = mod;
                        n = idx[i];
                        mod = mod[n];
                    }
                }
            }
            lastMod[n] = value;
        };
        return VarStoreImpl;
    }());
    scropt.VarStoreImpl = VarStoreImpl;
    /**
     * Data binding
     */
    var DataBinding;
    (function (DataBinding) {
        DataBinding.ParsedType = InnerParser.ParsedType;
        var attributes = {
            // model / values
            model: [InnerParser.varName],
            value: [InnerParser.varName, InnerParser.boolean, InnerParser.arithmetic],
            for: [InnerParser.forOf],
            // event functions
            click: [InnerParser.func, InnerParser.noReturn],
            change: [InnerParser.func, InnerParser.noReturn],
            focus: [InnerParser.func, InnerParser.noReturn],
            blur: [InnerParser.func, InnerParser.noReturn],
            keyup: [InnerParser.func, InnerParser.noReturn],
            keydown: [InnerParser.func, InnerParser.noReturn],
            keypress: [InnerParser.func, InnerParser.noReturn],
            // additional
            show: [InnerParser.boolean],
            hide: [InnerParser.boolean],
            disabled: [InnerParser.boolean]
        };
        var ProxyImplWithObserver = (function () {
            function ProxyImplWithObserver(target, handler) {
                this.target = target;
                this.handler = handler;
                if (Array.isArray(target)) {
                    this.arrayObserver(this);
                }
                else if (typeof target == "object") {
                    this.objectObserver(this);
                }
                else {
                }
                return target;
            }
            ProxyImplWithObserver.prototype.objectObserver = function (_this) {
                var observe = Object["observe"];
                observe(this.target, this.observerFn = function (changes) {
                    changes.forEach(function (change) {
                        var property = change.name;
                        var receiver = change.object;
                        switch (change.type) {
                            case "add":
                                _this.fnSet(property, receiver);
                                break;
                            case "update":
                                _this.fnSet(property, receiver);
                                break;
                            case "delete":
                                _this.fnDelete(property);
                                break;
                        }
                    });
                });
            };
            ProxyImplWithObserver.prototype.arrayObserver = function (_this) {
                var observe = Array["observe"];
                observe(this.target, this.observerFn = function (changes) {
                    changes.forEach(function (change) {
                        var property = change.name;
                        var receiver = change.object;
                        switch (change.type) {
                            case "add":
                                _this.fnSet(property, receiver);
                                break;
                            case "update":
                                _this.fnSet(property, receiver);
                                break;
                            case "delete":
                                _this.fnDelete(property);
                                break;
                            case "splice":
                                _this.fnSplice(receiver, change.index, change.removed, change.addedCount);
                                break;
                        }
                    });
                });
            };
            ProxyImplWithObserver.prototype.fnSet = function (property, receiver) {
                if (this.handler.set) {
                    if (receiver.hasOwnProperty(property)) {
                        var value = receiver[property];
                        if (!(value instanceof ProxyImplWithObserver)) {
                            this.handler.set(this.target, property, value, receiver, true); // 5th param 'true' is not official, it's an indicator for the custom proxy handler that this is coming from an observer
                        }
                    }
                }
            };
            ProxyImplWithObserver.prototype.fnSplice = function (receiver, index, removed, addedCount) {
                for (var i = 0; i < removed.length; i++) {
                    this.fnDelete(index + i);
                }
                for (var i = 0; i < addedCount; i++) {
                    this.fnSet(index + i, receiver);
                }
            };
            ProxyImplWithObserver.prototype.fnDelete = function (property) {
                if (this.handler.deleteProperty) {
                    this.handler.deleteProperty(this.target, property, true); // 3rd param 'true' is not official, it's an indicator for the custom proxy handler that this is coming from an observer
                }
            };
            return ProxyImplWithObserver;
        }());
        var Proxy = undefined;
        function init() {
            Proxy = window["Proxy"];
            if (Proxy == undefined) {
                if (Object["observe"] != undefined) {
                    var observe = Object["observe"];
                    Proxy = window["Proxy"] = ProxyImplWithObserver;
                }
                else {
                    // FIXME should do something, e.g. use requestAnimationFrame() and do dirty checking
                    alert("This browser is not supported yet, I'm sorry :(");
                    return;
                }
            }
        }
        DataBinding.init = init;
        function parse(type, value, varstore) {
            var parsers = attributes[type];
            if (parsers && Array.isArray(parsers)) {
                for (var _i = 0, parsers_1 = parsers; _i < parsers_1.length; _i++) {
                    var parser = parsers_1[_i];
                    var result = parser(value, varstore);
                    if (result !== null) {
                        return result;
                    }
                }
            }
            return null;
        }
        DataBinding.parse = parse;
        function parseDirect(type, value, varstore) {
            switch (type) {
                case DataBinding.ParsedType.BOOLEAN:
                    return InnerParser.boolean(value, varstore);
            }
        }
        DataBinding.parseDirect = parseDirect;
        (function (ChangeType) {
            ChangeType[ChangeType["Update"] = 0] = "Update";
            ChangeType[ChangeType["Delete"] = 1] = "Delete";
        })(DataBinding.ChangeType || (DataBinding.ChangeType = {}));
        var ChangeType = DataBinding.ChangeType;
        function isSimpleObserver(fn) {
            return typeof fn === typeof Function;
        }
        DataBinding.isSimpleObserver = isSimpleObserver;
        var Model = (function () {
            function Model(value, parent, real) {
                if (parent === void 0) { parent = undefined; }
                if (real === void 0) { real = true; }
                this.observers = [];
                this.children = {};
                this.parent = parent;
                this.value = value;
                this.real = real;
            }
            /*public notify() {
             for (var p in this.observers) {
             if (this.observers.hasOwnProperty(p)) {
             //this.observers[p]();
             }
             }
             }*/
            Model.prototype.notifyUpdate = function (target, property, newValue) {
                var stopPropagation = false;
                for (var p in this.observers) {
                    if (this.observers.hasOwnProperty(p)) {
                        var ret = this.observers[p].onModelUpdate(target, property, newValue);
                        if (ret) {
                            stopPropagation = true;
                        }
                    }
                }
                if (this.parent && !stopPropagation) {
                    this.parent.notifyUpdate(target, property, newValue);
                }
            };
            Model.prototype.notifyDelete = function (target, property) {
                //var stopPropagation = false;
                for (var p in this.observers) {
                    if (this.observers.hasOwnProperty(p)) {
                        var ret = this.observers[p].onModelDelete(target, property);
                    }
                }
                /*if (this.parent && !stopPropagation) {
                 this.parent.notifyDelete();
                 }*/
            };
            Model.prototype.get = function (path) {
                var varNameObj = InnerParser.varName(path).value;
                var n = varNameObj.name;
                var sub = varNameObj;
                var idx = sub.idx;
                var lastObj;
                var obj = this.value;
                while (sub = sub.sub) {
                    lastObj = obj;
                    obj = obj[n];
                    if (Array.isArray(idx) && idx.length > 0) {
                        while (idx.length > 1) {
                            var id = idx.shift();
                            obj = obj[id];
                        }
                        obj = obj[idx[0]];
                    }
                    n = sub.name;
                    idx = sub.idx;
                }
                if (Array.isArray(idx) && idx.length > 0) {
                    obj = obj[n];
                    while (idx.length > 1) {
                        var id = idx.shift();
                        obj = obj[id];
                    }
                    return obj[idx[0]];
                }
                else {
                    return obj[n];
                }
            };
            Model.prototype.update = function (path, value) {
                var varNameObj = InnerParser.varName(path).value;
                var n = varNameObj.name;
                var sub = varNameObj;
                var idx = sub.idx;
                var lastObj;
                var obj = this.value;
                while (sub = sub.sub) {
                    lastObj = obj;
                    obj = obj[n];
                    if (Array.isArray(idx) && idx.length > 0) {
                        while (idx.length > 1) {
                            var id = idx.shift();
                            obj = obj[id];
                        }
                        obj = obj[idx[0]];
                    }
                    n = sub.name;
                    idx = sub.idx;
                }
                if (Array.isArray(idx) && idx.length > 0) {
                    obj = obj[n];
                    while (idx.length > 1) {
                        var id = idx.shift();
                        obj = obj[id];
                    }
                    obj[idx[0]] = value;
                }
                else {
                    obj[n] = value;
                }
            };
            Model.prototype.delete = function (path) {
                var varNameObj = InnerParser.varName(path).value;
                var n = varNameObj.name;
                var sub = varNameObj;
                var idx = sub.idx;
                var lastObj;
                var obj = this.value;
                while (sub = sub.sub) {
                    lastObj = obj;
                    obj = obj[n];
                    if (Array.isArray(idx) && idx.length > 0) {
                        while (idx.length > 1) {
                            var id = idx.shift();
                            obj = obj[id];
                        }
                        obj = obj[idx[0]];
                    }
                    n = sub.name;
                    idx = sub.idx;
                }
                if (Array.isArray(idx) && idx.length > 0) {
                    obj = obj[n];
                    while (idx.length > 1) {
                        var id = idx.shift();
                        obj = obj[id];
                    }
                    delete obj[idx[0]];
                }
                else {
                    delete obj[n];
                }
            };
            Model.prototype.observe = function (path, fn) {
                var obj = InnerParser.varName(path).value;
                //console.log('observing', obj);
                var n = obj.name;
                var sub = obj;
                var idx = sub.idx;
                var lastModel;
                var mod = this.children[n];
                if (!mod) {
                    var val = undefined;
                    mod = this.children[n] = new Model(val, this);
                }
                /*if (idx) {
                 for (var i = 0; i < idx.length; i++) {
                 n = idx[i];
                 if (mod.children[n]) {
                 mod = mod.children[n] = new Model(undefined, mod);
                 }
                 }
                 }*/
                while (sub = sub.sub) {
                    n = sub.name;
                    lastModel = mod;
                    mod = mod.children[n];
                    idx = sub.idx; // FIXME it's unused now
                    var val = undefined;
                    if (!mod) {
                        mod = lastModel.children[n] = new Model(val, lastModel);
                    }
                }
                if (isSimpleObserver(fn)) {
                    mod.observe_OLD({
                        onModelUpdate: function (target, property, newValue) {
                            return fn(ChangeType.Update, target, property, newValue);
                        },
                        onModelDelete: function (target, property) {
                            return fn(ChangeType.Delete, target, property);
                        }
                    });
                }
                else {
                    mod.observe_OLD(fn);
                }
                //lastMod[n] = value;
            };
            Model.prototype.observe_OLD = function (fn) {
                this.observers.push(fn);
            };
            Model.prototype.unobserve_OLD = function (fn) {
                for (var i = 0; i < this.observers.length; i++) {
                    if (this.observers[i] == fn) {
                        this.observers.splice(i, 1);
                        break;
                    }
                }
            };
            Model.createModel = function (base) {
                if (base === void 0) { base = {}; }
                //var base = {};
                var m = new Model(base);
                m.base = base;
                this.create(m);
                return m;
            };
            /**
             * Deletes observers in the given model and all of its children.
             * @param model
             */
            Model.deleteObservers = function (property, model) {
                //console.log('deleteObservers', model);
                if (model === undefined) {
                    return;
                }
                var obj = model.value;
                $.each(model.observers, function (i, item) {
                    item.onModelDelete(obj, property);
                });
                //console.log('obj: ', obj);
                for (var p in model.children) {
                    //console.log('prop: ', p);
                    if (model.children.hasOwnProperty(p)) {
                        var child = model.children[p];
                        if (child !== undefined) {
                            Model.deleteObservers(p, child);
                        }
                    }
                }
            };
            Model.isArrayIndex = function (val) {
                return !isNaN(val) && 0 === val % 1 && val >= 0;
            };
            Model.create = function (model) {
                var obj = model.value;
                if (!Array.isArray(obj) && typeof obj != "object") {
                    return obj;
                }
                for (var p in obj) {
                    if (obj.hasOwnProperty(p)) {
                        var propModel = new Model(obj[p], model);
                        // TODO test for removing observers if a children gets overwritten
                        // FIXME might be better to merge the new and old children if possible
                        Model.deleteObservers(p, model.children[p]);
                        model.children[p] = propModel;
                        obj[p] = Model.create(propModel);
                    }
                }
                return model.value = new Proxy(obj, {
                    set: function (target, property, value, receiver) {
                        if (arguments.length <= 4) {
                            var propModel;
                            var child = model.children[property];
                            //console.log('child', child, target);
                            if (child) {
                                model.children[property].value = value;
                                propModel = model.children[property];
                            }
                            else {
                                model.children[property] = propModel = new Model(value, model);
                            }
                            //propModel.observers = (model.children[property]) ? model.children[property].observers : []; // TODO
                            //model.children[property] = propModel;
                            target[property] = Model.create(propModel);
                        }
                        else {
                            // THIS WAS HERE EARLIER, BUT I DON'T REMEMBER WHY...
                            // if (Array.isArray(value) || typeof value == "object") {
                            var propModel;
                            if (model.children[property]) {
                                model.children[property].value = value;
                                propModel = model.children[property];
                            }
                            else {
                                model.children[property] = propModel = new Model(value, model);
                            }
                            //model.children[property] = propModel;
                            target[property] = Model.create(propModel);
                        }
                        if (Array.isArray(target) && Model.isArrayIndex(property)) {
                            model.notifyUpdate(target, property, value); // TODO check this
                        }
                        else if (model.children[property]) {
                            model.children[property].notifyUpdate(target, property, value);
                        }
                        //console.debug("'" + property + "'", "value set:", value);
                        return true;
                    },
                    deleteProperty: function (target, property) {
                        if (arguments.length <= 2) {
                            delete target[property];
                        }
                        if (Array.isArray(target) && Model.isArrayIndex(property)) {
                            model.notifyDelete(target, property);
                        }
                        else if (model.children[property]) {
                            model.children[property].notifyDelete();
                        }
                        delete model.children[property];
                        //console.debug(property, "deleted");
                        return true;
                    }
                });
            };
            return Model;
        }());
        DataBinding.Model = Model;
        // Element methods
        function getScroptAttributes(elem) {
            var attrs = {};
            $.each(attributes, function (attr, fns) {
                var value = $(elem).attr(scropt.SC_ATTR_SC_PRE + attr) || $(elem).attr(scropt.SC_ATTR_DATA_SC_PRE + attr);
                if (value !== undefined) {
                    attrs[attr] = value;
                }
            });
            return attrs;
        }
        DataBinding.getScroptAttributes = getScroptAttributes;
        function getAttributes_OLD(elem) {
            var realElem;
            if (typeof elem == "string") {
                // classic selector
                realElem = $(elem);
            }
            else {
                realElem = $(elem.selector);
            }
        }
        DataBinding.getAttributes_OLD = getAttributes_OLD;
        /**
         * Finds all elements that needs to be processed by scropt.
         */
        function findScroptElements(container, model) {
            if (model === undefined || model === null) {
                return;
            }
            var varstore = new VarStoreImpl(model);
            // $('[sc-model]:not(.sc-upgrade)');
            var str = Object.keys(attributes).map(function (i) { return ("[" + scropt.SC_ATTR_SC_PRE + i + "]:not(." + scropt.SC_CLASS_UPGRADED + "),[" + scropt.SC_ATTR_DATA_SC_PRE + i + "]:not(." + scropt.SC_CLASS_UPGRADED + ")"); }).join(',');
            //console.log('findScroptElements', 'str:', str);
            $(container).find(str).addBack(str).each(function (i, item) {
                var attrs = getScroptAttributes($(item));
                //console.log('findScroptElements', 'attrs:', attrs, $(item).val);
                for (var attr in attrs) {
                    switch (attr) {
                        case 'model':
                            handleModel(attrs[attr], item, varstore);
                            break;
                    }
                }
                //$(item).val('akarmi');
            });
        }
        DataBinding.findScroptElements = findScroptElements;
        function handleModel(path, elem, varstore) {
            var jqElem = $(elem);
            var model = varstore.getModel();
            var defValue = model.get(path);
            if (elem instanceof HTMLInputElement) {
                switch (elem.type) {
                    case 'checkbox':
                        jqElem.prop('checked', defValue == jqElem.val());
                        jqElem.trigger('change');
                        model.observe(path, function (type, target, prop, newValue) {
                            jqElem.prop('checked', newValue == jqElem.val());
                            jqElem.trigger('change');
                        });
                        jqElem.on('change', function (e) {
                            if (e.originalEvent) {
                                if (jqElem.prop('checked')) {
                                    model.update(path, jqElem.val());
                                }
                                else {
                                    model.update(path, undefined);
                                }
                            }
                        });
                        break;
                    case 'radio':
                        jqElem.prop('checked', defValue == jqElem.val());
                        jqElem.trigger('change');
                        model.observe(path, function (type, target, prop, newValue) {
                            jqElem.prop('checked', newValue == jqElem.val());
                            jqElem.trigger('change');
                        });
                        jqElem.on('change', function (e) {
                            if (e.originalEvent) {
                                model.update(path, jqElem.val());
                            }
                        });
                        break;
                    default:
                        jqElem.val(defValue);
                        jqElem.trigger('input');
                        model.observe(path, function (type, target, prop, newValue) {
                            if (newValue !== jqElem.val()) {
                                jqElem.val(newValue);
                                jqElem.trigger('input');
                            }
                        });
                        jqElem.on('input', function (e) {
                            if (e.originalEvent) {
                                model.update(path, jqElem.val());
                            }
                        });
                        break;
                }
            }
            else if (elem instanceof HTMLSelectElement || elem instanceof HTMLTextAreaElement) {
                jqElem.val(defValue);
                jqElem.trigger('input');
                model.observe(path, function (type, target, prop, newValue) {
                    if (newValue !== jqElem.val()) {
                        jqElem.val(newValue);
                        jqElem.trigger('input');
                    }
                });
                jqElem.on('input', function (e) {
                    if (e.originalEvent) {
                        model.update(path, jqElem.val());
                    }
                });
            }
            else {
                jqElem.html(defValue);
                jqElem.trigger('input');
                model.observe(path, function (type, target, prop, newValue) {
                    if (newValue !== jqElem.html()) {
                        jqElem.html(newValue || '');
                        jqElem.trigger('input');
                    }
                });
                jqElem.on('input', function (e) {
                    if (e.originalEvent) {
                        model.update(path, jqElem.html());
                    }
                });
            }
        }
    })(DataBinding = scropt.DataBinding || (scropt.DataBinding = {}));
    /**
     * IO module which includes Server-Sent Events and WebSockets
     */
    var IO;
    (function (IO) {
        var CommonConnector = (function () {
            function CommonConnector(url) {
                this.url = url;
            }
            CommonConnector.prototype.listen = function (type, callback) {
                if (!this.listeners[type]) {
                    this.listeners[type] = [];
                }
                this.listeners[type].push(callback);
                if (this.listeners[type].length === 1) {
                    this.registerEvent(type);
                }
            };
            CommonConnector.prototype.unlisten = function (callback) {
                for (var p in this.listeners) {
                    if (this.listeners.hasOwnProperty(p)) {
                        var idx = -1;
                        this.listeners[p].forEach(function (handler, i) {
                            if (handler === callback) {
                                idx = i;
                                return false;
                            }
                        });
                        if (idx > -1) {
                            this.listeners[p].splice(idx, 1);
                            this.unregisterEvent(p);
                            break;
                        }
                    }
                }
            };
            CommonConnector.prototype.onOpen = function (callback) {
                this.listen('open', callback);
            };
            CommonConnector.prototype.onError = function (callback) {
                this.listen('error', callback);
            };
            CommonConnector.prototype.onMessage = function (callback) {
                this.listen('message', callback);
            };
            CommonConnector.prototype.onClose = function (callback) {
                this.listen('close', callback);
            };
            CommonConnector.prototype.registerEvent = function (event) {
                if (this.source) {
                    // FIXME this should check whether the event has benn registered already
                    this.source.addEventListener(event, this.internalReceive, false);
                }
            };
            CommonConnector.prototype.unregisterEvent = function (event) {
                if (this.listeners[event].length > 0) {
                    return;
                }
                delete this.listeners[event];
                if (this.source) {
                    this.source.removeEventListener(event);
                }
            };
            CommonConnector.prototype.internalReceive = function (event) {
                var handlers;
                if ((handlers = this.listeners[event.type]) !== undefined) {
                    handlers.forEach(function (handler) {
                        handler(event);
                    });
                }
            };
            return CommonConnector;
        }());
        var SSE = (function (_super) {
            __extends(SSE, _super);
            function SSE(url, external) {
                if (external === void 0) { external = false; }
                _super.call(this, url);
                this.external = external;
            }
            SSE.prototype.connect = function () {
                if (SSE.isAvailable()) {
                    this.source = (external) ? new EventSource(this.url, { withCredentials: true }) : new EventSource(this.url);
                    for (var p in this.listeners) {
                        if (this.listeners.hasOwnProperty(p)) {
                            this.registerEvent(p);
                        }
                    }
                }
                else {
                    alert("Sorry, your browser doesn't support Server-Sent Events yet :(");
                }
            };
            SSE.prototype.close = function () {
                if (SSE.isAvailable()) {
                    if (this.source) {
                        this.source.close();
                        this.internalReceive({ type: 'close' }); // little hack
                        this.source = null;
                    }
                }
                else {
                    alert("Sorry, your browser doesn't support Server-Sent Events yet :(");
                }
            };
            SSE.isAvailable = function () {
                return "EventSource" in window;
            };
            return SSE;
        }(CommonConnector));
        IO.SSE = SSE;
        var WebSockets = (function (_super) {
            __extends(WebSockets, _super);
            function WebSockets(url, protocols) {
                _super.call(this, url);
                this.protocols = protocols;
            }
            WebSockets.prototype.connect = function () {
                if (WebSockets.isAvailable()) {
                    this.source = new WebSocket(this.url, this.protocols);
                    for (var p in this.listeners) {
                        if (this.listeners.hasOwnProperty(p)) {
                            this.registerEvent(p);
                        }
                    }
                }
                else {
                    throw new Error('WebSocket not supported');
                }
            };
            WebSockets.prototype.close = function (code, reason) {
                if (WebSockets.isAvailable()) {
                    if (this.source) {
                        this.source.close(code, reason);
                        this.internalReceive({ type: 'close' });
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    alert("Sorry, your browser doesn't support Server-Sent Events yet :(");
                }
            };
            WebSockets.prototype.send = function (data) {
                if (WebSockets.isAvailable()) {
                    if (this.source && this.source.readyState == WebSocket.OPEN) {
                        if (typeof data === typeof Object) {
                            data = JSON.stringify(data);
                        }
                        this.source.send(data);
                        return WebSocket.OPEN;
                    }
                    else {
                        return (this.source) ? this.source.readyState : WebSocket.CLOSED;
                    }
                }
                else {
                    alert("Sorry, your browser doesn't support Server-Sent Events yet :(");
                }
            };
            WebSockets.prototype.getActualProtocol = function () {
                return this.source.protocol;
            };
            WebSockets.isAvailable = function () {
                return "WebSocket" in window;
            };
            return WebSockets;
        }(CommonConnector));
        IO.WebSockets = WebSockets;
    })(IO = scropt.IO || (scropt.IO = {}));
    /**
     * Notification API implementation
     * See more at https://developer.mozilla.org/en-US/docs/Web/API/notification
     */
    var Notifications;
    (function (Notifications) {
        var Notification = window["Notification"];
        function init(callback) {
            if (isAvailable()) {
                requestPermission(callback);
            }
        }
        Notifications.init = init;
        function requestPermission(callback) {
            if (Notification.permission !== 'denied') {
                Notification.requestPermission(callback);
            }
        }
        function notify(title, message, icon, onclick, tag) {
            if (!isAvailable()) {
                return false;
            }
            // TODO requestPermission(); might be used to ask for permission before delivering the notification
            if (Notification.permission !== "granted") {
                return false;
            }
            var nTag = tag || "notification";
            var n = new Notification(title, { tag: nTag, body: message, icon: icon });
            if (onclick && $.isFunction(onclick)) {
                n.onclick = onclick;
            }
            return true;
        }
        Notifications.notify = notify;
        function isAvailable() {
            return Notification !== undefined;
        }
        Notifications.isAvailable = isAvailable;
    })(Notifications = scropt.Notifications || (scropt.Notifications = {}));
    /**
     * Page Visibility API implementation
     * See more at https://developer.mozilla.org/en-US/docs/Web/API/Page_Visibility_API
     */
    var PageVisibility;
    (function (PageVisibility) {
        var hidden, visibilityChange;
        var isPageVisible;
        var listeners = [];
        // running some initializations
        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
        }
        else if (typeof document["mozHidden"] !== undefined) {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
        }
        else if (typeof document["msHidden"] !== undefined) {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
        }
        else if (typeof document["webkitHidden"] !== undefined) {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
        }
        var handleVisibilityChange = function () {
            isPageVisible = !document[hidden];
            listeners.forEach(function (handler) {
                handler(isPageVisible);
            });
        };
        if (typeof document.addEventListener !== undefined && typeof document[hidden] !== undefined) {
            document.addEventListener(visibilityChange, handleVisibilityChange, false);
        }
        // end of inits
        function listen(callback) {
            for (var i = 0; i < listeners.length; i++) {
                if (listeners[i] === callback) {
                    return;
                }
            }
            listeners.push(callback);
        }
        PageVisibility.listen = listen;
        function unlisten(callback) {
            for (var i = 0; i < listeners.length; i++) {
                if (listeners[i] === callback) {
                    listeners.splice(i, 1);
                    return true;
                }
            }
            return false;
        }
        PageVisibility.unlisten = unlisten;
        function clear() {
            listeners.splice(0, listeners.length);
        }
        PageVisibility.clear = clear;
        function isVisible() {
            return isPageVisible;
        }
        PageVisibility.isVisible = isVisible;
    })(PageVisibility = scropt.PageVisibility || (scropt.PageVisibility = {}));
    /**
     * Routing
     */
    var InternalRouting;
    (function (InternalRouting) {
    })(InternalRouting || (InternalRouting = {}));
    var Routing;
    (function (Routing) {
        var RouteNode = (function () {
            function RouteNode(expression, parent) {
                this.children = [];
                /**
                 * Used if * is the end of path.
                 */
                this.terminal = undefined;
                this.params = [];
                if (expression !== undefined) {
                    var processed = RouteNode.processExpression(expression);
                    this.expression = processed.expression;
                    this.params = processed.params;
                }
                this.parent = parent;
            }
            RouteNode.createFromProcessed = function (data, parent) {
                var node = new RouteNode(undefined, parent);
                node.expression = data.expression;
                node.params = data.params;
                return node;
            };
            RouteNode.processExpression = function (expression) {
                // good: \{([a-zA-Z0-9]+)(?:\:((?:int|number|word|any)+))?\}
                var res;
                var offset = 0;
                var newExp = "^";
                var params = [];
                while ((res = RouteNode.PARAM_REGEXP.exec(expression)) != null) {
                    var match = res[0];
                    var param = { name: res[1], type: RouteNodeParamType.Any };
                    // Updating the new expression
                    newExp += escapeRegExp(expression.substring(offset, res.index));
                    offset = res.index + match.length;
                    // Setting the param's type
                    switch (res[2]) {
                        case "int":
                            // (\\-?[0-9]+)
                            param.type = RouteNodeParamType.Int;
                            newExp += "(\\-?[0-9]+)";
                            break;
                        case "number":
                            // (\-?[0-9]+(?:\.[0-9]*)?|\-?\.[0-9]+)
                            param.type = RouteNodeParamType.Number;
                            newExp += "(\\-?[0-9]+(?:\\.[0-9]*)?|\\-?\\.[0-9]+)";
                            break;
                        case "word":
                            // (\w+)
                            param.type = RouteNodeParamType.Word;
                            newExp += "(\\w+)";
                            break;
                        default:
                            // (.+)
                            newExp += "(.+)";
                            break;
                    }
                    params.push(param);
                }
                newExp += escapeRegExp(expression.substring(offset, expression.length)) + "$";
                return { params: params, expression: new RegExp(newExp, "g") };
            };
            RouteNode.prototype.addChild = function (expression) {
                return this.findOrAddChild(expression, true);
            };
            RouteNode.prototype.findChild = function (expression) {
                return this.findOrAddChild(expression, false);
            };
            RouteNode.prototype.findOrAddChild = function (expression, createIfNotExists) {
                if (createIfNotExists === void 0) { createIfNotExists = false; }
                var processed = RouteNode.processExpression(expression);
                for (var i in this.children) {
                    var child = this.children[i];
                    if (child.expression.source == processed.expression.source) {
                        return child;
                    }
                }
                if (createIfNotExists) {
                    var node = RouteNode.createFromProcessed(processed, this);
                    this.children.push(node);
                    return node;
                }
                return null;
            };
            RouteNode.prototype.getDataByPath = function (path) {
                for (var i in this.children) {
                    var child = this.children[i];
                    var match;
                    if ((match = child.expression.exec(path)) != null) {
                        var params = {};
                        for (var j = 0; j < child.params.length; j++) {
                            params[child.params[j].name] = match[j + 1];
                        }
                        resetRegExp(child.expression);
                        return { params: params, child: child };
                    }
                }
                return null;
            };
            RouteNode.PARAM_REGEXP = new RegExp("\\{([a-zA-Z0-9]+)(?:\\:((?:int|number|word|any)+))?\\}", "g");
            return RouteNode;
        }());
        /**
         * Configuration module
         */
        var Config;
        (function (Config) {
            var basePath = "/";
            function base(path) {
                if (!path) {
                    path = "/";
                }
                routeConfig.base = path.trim();
                return this;
            }
            Config.base = base;
            function getBasePath() {
                return basePath;
            }
        })(Config = Routing.Config || (Routing.Config = {}));
        /**
         * Global retrieve / error handlers.s
         */
        var Global;
        (function (Global) {
            function before(callback) {
                globalHandlers.before = callback;
            }
            Global.before = before;
            function done(callback) {
                globalHandlers.done = callback;
            }
            Global.done = done;
            function fail(callback) {
                globalHandlers.fail = callback;
            }
            Global.fail = fail;
        })(Global = Routing.Global || (Routing.Global = {}));
        var RouteNodeParamType;
        (function (RouteNodeParamType) {
            RouteNodeParamType[RouteNodeParamType["Int"] = 0] = "Int";
            RouteNodeParamType[RouteNodeParamType["Number"] = 1] = "Number";
            RouteNodeParamType[RouteNodeParamType["Word"] = 2] = "Word";
            RouteNodeParamType[RouteNodeParamType["Any"] = 3] = "Any";
        })(RouteNodeParamType || (RouteNodeParamType = {}));
        var globalHandlers = {
            before: undefined,
            done: undefined,
            fail: undefined
        };
        var routeConfig = {
            base: "/"
        };
        var rootNode = new RouteNode();
        var pageControllers = {};
        var isEnabled = false;
        /**
         * PUBLIC methods
         */
        /**
         * Format for parameters:
         * {paramName[:type]}
         *
         * Type can be:
         * - int: integer
         * - number: decimal number
         * - word: alphanumeric word matching the following regexp: [A-Za-z0-9_]
         * - any (default): matches everything except '/'
         *
         * For example:
         *
         * /page/{id:int}
         * /page/{id:number}/edit
         * /search/{query}
         * /page/can_be_like_this{id:number}
         * @param path the URL
         * @param controller
         * @returns {scropt.Routing.route}
         */
        function route(path, controller) {
            if (!path) {
                throw new Error("The path must be valid.");
            }
            if (!isEnabled) {
                enable(true);
            }
            var currentNode = rootNode;
            var datas = path.split('/');
            for (var i = 0; i < datas.length; i++) {
                var piece = datas[i];
                if (piece !== undefined && piece !== null) {
                    piece = piece.trim();
                    switch (piece) {
                        case '':
                            break;
                        case '*':
                            var terminal = new RouteNode(undefined, currentNode);
                            currentNode.terminal = terminal;
                            currentNode = terminal;
                            break;
                        default:
                            currentNode = currentNode.addChild(piece);
                    }
                }
            }
            controller.path = path;
            currentNode.controller = controller;
            return this;
        }
        Routing.route = route;
        function otherwise(controller) {
            // TODO this is not good because the matcher splits the URL by '/' so if there's a URL like 'one/two/three/', it's not gonna match
            route("/*", controller);
            return this;
        }
        Routing.otherwise = otherwise;
        function redirect(path) {
            // TODO
            /*if (InternalNavigation.isHistorySupported()) {
             window.history.replaceState(null, '', path);
             }*/
        }
        Routing.redirect = redirect;
        function page(name, controller) {
            if (pageControllers[name] !== undefined) {
                throw new Error("'" + name + "' is already defined as a page controller.");
            }
            if (typeof controller !== "function") {
                pageControllers[name] = controller;
            }
            else {
                pageControllers[name] = {};
                pageControllers[name].done = function (response, jqXHR) {
                    controller.call(this, response, true, jqXHR);
                };
                pageControllers[name].fail = function (response, jqXHR) {
                    controller.call(this, response, false, jqXHR);
                };
            }
            return this;
        }
        Routing.page = page;
        function enable(enable) {
            isEnabled = enable;
            if (isEnabled) {
                $(function () {
                    // TODO
                    //console.log(window.location);
                    // TODO handleURL(window.location.pathname);
                });
            }
        }
        Routing.enable = enable;
        /**
         * PRIVATE methods
         */
        /**
         *
         * @param path
         * @returns RouteNode null if the given path is not registered; a valid RouteNode otherwise
         */
        function getRouteNode(path) {
            var currentNode = rootNode;
            var datas = path.split('/');
            var params = {};
            for (var i = 0; i < datas.length; i++) {
                var piece = datas[i];
                piece = piece.trim();
                if (piece !== "") {
                    var nodeData = currentNode.getDataByPath(piece);
                    if (nodeData == null && !currentNode.terminal) {
                        return null;
                    }
                    else if (nodeData == null && currentNode.terminal) {
                        currentNode = currentNode.terminal;
                        $.extend(params, { '*': datas.slice(i, datas.length).join('/') });
                        break;
                    }
                    currentNode = nodeData.child;
                    $.extend(params, nodeData.params);
                }
            }
            return { params: params, target: currentNode };
        }
        function handleURL(path, popstate) {
            var node = getRouteNode(path);
            if (node != null) {
                var pathController = Object.create(node.target.controller);
                var pageController;
                var model = (popstate && popstate.model) ? DataBinding.Model.createModel(popstate.model) : DataBinding.Model.createModel();
                if (pathController.controller) {
                    pageController = Object.create(pageControllers[pathController.controller]);
                    pageController.params = node.params;
                    // setting the model
                    pageController.model = model.value;
                }
                if (pathController.url) {
                    if (globalHandlers.before && globalHandlers.before(pathController.url, node.params) === false) {
                        return;
                    }
                    for (var p in node.params) {
                        pathController.url = pathController.url.replace(new RegExp("{" + p + "}", "g"), node.params[p]);
                    }
                    if (pageController && pageController.before && pageController.before(pathController.url) === false) {
                        return;
                    }
                    var ajaxSettings = { data: node.params, context: pageController };
                    if (pathController.settings) {
                        $.extend(ajaxSettings, pathController.settings);
                    }
                    var promises = [];
                    var urlPromise = new Promise(function (resolve, reject) {
                        $.ajax(pathController.url, ajaxSettings).done(function (data, textStatus, jqXHR) {
                            resolve({
                                type: 'url',
                                data: data,
                                xhr: jqXHR
                            });
                        }).fail(function (jqXHR) {
                            reject({
                                type: 'url',
                                xhr: jqXHR
                            });
                        });
                    });
                    promises.push(urlPromise);
                    if (pageController.templateUrl) {
                        promises.push(new Promise(function (resolve, reject) {
                            // TODO first we should try to load from cache
                            $.get(pageController.templateUrl + "?" + Date.now()) // bypassing caching
                                .done(function (data, textStatus, jqXHR) {
                                resolve({
                                    type: 'template',
                                    data: data,
                                    xhr: jqXHR
                                });
                            })
                                .fail(function (jqXHR, textStatus, errorThrown) {
                                reject({
                                    type: 'template',
                                    xhr: jqXHR
                                });
                            });
                        }));
                    }
                    // here we load the normal url and the template, if needed
                    Promise.all(promises)
                        .then(function (values) {
                        if (globalHandlers.done && globalHandlers.done(node.params, values[0]['data'], values[0]['xhr']) === false) {
                            return;
                        }
                        var obj = {
                            path: path,
                            controller: pageController,
                            model: model
                        };
                        if (values.length === 1) {
                            // url only, but we need to check template anyways
                            if (pageController.template) {
                                responseWithUrlAndTemplate(obj, values[0], pageController.template, popstate);
                            }
                            else {
                                responseWithUrlAndTemplate(obj, values[0], null, popstate);
                            }
                        }
                        else {
                            // url + templateURL
                            responseWithUrlAndTemplate(obj, values[0], values[1], popstate);
                        }
                    })
                        .catch(function (reason) {
                        console.log('ERROR', reason);
                        var jqXHR = reason.xhr;
                        if (globalHandlers.fail && globalHandlers.fail(jqXHR.status, jqXHR.responseText, jqXHR) === false) {
                            return;
                        }
                        pageController.fail(jqXHR.responseText, jqXHR);
                    });
                }
                else {
                }
            }
            else {
            }
        }
        Routing.handleURL = handleURL;
        function handle(url) {
            handleURL(url.trim().replace(new RegExp("^" + routeConfig.base), ""));
        }
        Routing.handle = handle;
        function responseWithUrlAndTemplate(obj, urlData, templateData, popstate) {
            //console.info('responseWithUrlAndTemplate', arguments);
            if (obj.controller) {
                obj.controller.done(urlData.data, urlData.jqXHR);
            }
            // FIXME this should be done if before the view is being applied but only if the view is really applied
            // FIXME also, this should be in 'fail' too if the user wants to show an error page, that should also have it
            // FIXME BETTER IDEA: the pagecontroller (and the model ofc) should be on the history stack
            //InternalRouting.currentPageController = obj.controller;
            //InternalRouting.currentPageController.model = obj.model;
            InternalNavigation.navigationFinished(obj, !!popstate);
            var view;
            if (templateData != null) {
                view = templateData.data;
                var modelData = (popstate && popstate.model) ? popstate.model : urlData.data;
                $.extend(true, obj.model.value, modelData);
            }
            else if (urlData != null) {
                view = urlData.data;
            }
            try {
                view = $('<div>' + view + '</div>');
                Renderer.render(view, obj.model);
                view = view.children();
            }
            catch (error) {
                console.error('render error', error);
            }
            // TODO apply the loaded page to the main view
            $('#page-target').html(view);
        }
    })(Routing = scropt.Routing || (scropt.Routing = {}));
    var Renderer;
    (function (Renderer) {
        /**
         *
         * @param container The elements to be rendered in. Note that this should be a root node, otherwise the top level nodes won't be processed.
         * @param model The model to be used for rendering.
         */
        function render(container, model) {
            var realContainer;
            if (!container) {
                realContainer = $(document);
            }
            else {
                realContainer = $(container);
            }
            FrontPartStorage.elements.forEach(function (elemHolder) {
                // TODO [REMOVED FOR TESTING] console.log("holder:", elemHolder, "selector:", elemHolder.element.selector);
                // TODO [REMOVED FOR TESTING] console.log(realContainer, realContainer.find(elemHolder.element.selector));
                var selector = elemHolder.element.selector + ":not(" + scropt.SC_CLASS_RENDERED + ")";
                realContainer.find(selector) /*.addBack(selector)*/.each(function (_, e) {
                    var elem = $(e);
                    //console.log("elem found:", elem);
                    if (!elem.hasClass(scropt.SC_CLASS_RENDERED)) {
                        //console.log("elem is not rendered yet", elem);
                        var newElem = elemHolder.init();
                        newElem.setModel(model);
                        var newElemObj = newElem.renderInternal(elem);
                        if (newElem.replace !== Elements.InsertMode.Manual) {
                            elem.addClass(scropt.SC_CLASS_RENDERED);
                            if (newElemObj) {
                                newElemObj.addClass(scropt.SC_CLASS_RENDERED);
                            }
                        }
                        if (newElemObj) {
                            initHandlers(newElemObj);
                            // TODO [REMOVED FOR TESTING] console.info(elem, newElem, newElemObj);
                            switch (newElem.replace) {
                                case Elements.InsertMode.Insert:
                                    elem.append(newElemObj);
                                    break;
                                case Elements.InsertMode.Replace:
                                    elem.replaceWith(newElemObj);
                                    break;
                                case Elements.InsertMode.ReplaceKeepContents:
                                    elem.wrapInner(newElemObj);
                                    newElemObj.unwrap();
                                    break;
                                case Elements.InsertMode.Wrap:
                                    elem.wrap(newElemObj);
                                    break;
                                case Elements.InsertMode.None:
                                    // do nothing
                                    break;
                            }
                        }
                    }
                });
                console.groupEnd();
            });
            // do some sc-* attribute processing after we have the new elements
            DataBinding.findScroptElements(realContainer, model);
        }
        Renderer.render = render;
        function initHandlers(elem) {
        }
    })(Renderer || (Renderer = {}));
    /**
     * Elements
     */
    var Elements;
    (function (Elements) {
        (function (InsertMode) {
            InsertMode[InsertMode["Insert"] = 0] = "Insert";
            InsertMode[InsertMode["Replace"] = 1] = "Replace";
            InsertMode[InsertMode["ReplaceKeepContents"] = 2] = "ReplaceKeepContents";
            InsertMode[InsertMode["Wrap"] = 3] = "Wrap";
            InsertMode[InsertMode["None"] = 4] = "None";
            InsertMode[InsertMode["Manual"] = 5] = "Manual";
        })(Elements.InsertMode || (Elements.InsertMode = {}));
        var InsertMode = Elements.InsertMode;
        function register(name, element) {
            // TODO [REMOVED FOR TESTING] console.log(name, element, typeof element);
            FrontPartStorage.addElement(name, element);
            /*if (element.hasOwnProperty('render') && typeof element !== typeof Function) {
             // it's a simple object
             FrontPartStorage.addElement(name, Object.create(element));
             } else {
             // it's an Element classname
             //FrontPartStorage.addElement(name, new (<typeof Element>element)());
             //FrontPartStorage.addElement(name, Object.create(element));
             FrontPartStorage.addElement(name, new (<typeof element>element)());
             }*/
            return this;
        }
        Elements.register = register;
        function copyAttributes(src, dst) {
            var exclude = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                exclude[_i - 2] = arguments[_i];
            }
            var srcAttrs = $(src)[0].attributes;
            var shouldExclude = exclude && exclude.length > 0;
            for (var i = 0; i < srcAttrs.length; i++) {
                var attr = srcAttrs.item(i);
                if (!shouldExclude || exclude.indexOf(attr.name)) {
                    if (attr.name !== "class") {
                        $(dst).attr(attr.name, attr.value);
                    }
                    else {
                        $(dst).addClass(attr.value);
                    }
                }
            }
        }
        Elements.copyAttributes = copyAttributes;
        function registerObserver(target, callback) {
            var jTarget = $(target);
            if ("MutationObserver" in window) {
                var config = { attributes: true, childList: true, characterData: true, subtree: true };
                for (var i = 0; i < jTarget.length; i++) {
                    var t = jTarget.get(i);
                    var observer = new MutationObserver(function (mutations) {
                        observer.disconnect();
                        // console.log(mutations);
                        if (callback !== undefined) {
                            callback(mutations);
                        }
                    });
                    observer.observe(t, config);
                }
            }
            else {
                var _loop_1 = function(i) {
                    var t = jTarget.get(i);
                    $(t).one('DOMSubtreeModified propertychange', function () {
                        if (callback !== undefined) {
                            callback({ origin: t });
                        }
                    });
                };
                for (var i = 0; i < jTarget.length; i++) {
                    _loop_1(i);
                }
            }
        }
        Elements.registerObserver = registerObserver;
        ;
        var Element = (function () {
            function Element() {
                this.replace = InsertMode.None;
                this.model = {};
                // FIXME ezeknek a PageController-ből kell jönnie!!!
                this.binding = DataBinding.Model.createModel();
                this.model = this.binding.value;
            }
            Element.prototype.setModel = function (model) {
                //console.log('>>> MODEL: ', model);
                if (model !== undefined) {
                    this.binding = model;
                    this.model = model.value;
                }
            };
            Element.prototype.findAncestor = function (match) {
                var selectors = undefined;
                if (this.parents !== undefined) {
                    if (typeof this.parents === "string") {
                        var p = this.parents.trim();
                        if (p.indexOf("!") === 0) {
                            // find the element by name and set its selector as the value of selectors
                            selectors = FrontPartStorage.elementByName(p.substring(1)).selector;
                        }
                        else {
                            selectors = p;
                        }
                    }
                    else {
                        var pArray = this.parents;
                        for (var i = 0; i < pArray.length; i++) {
                            if (pArray[i].trim().indexOf("!") === 0) {
                                // find the element by name and set its selector as the value of selectors
                                pArray[i] = FrontPartStorage.elementByName(pArray[i].substring(1)).selector;
                            }
                        }
                        selectors = pArray.join(",");
                    }
                    if (selectors !== undefined) {
                        var elem;
                        var par = $(match).parent();
                        if (par.length > 0) {
                            elem = $(match).closest(selectors, par.get(0));
                        }
                        else {
                            elem = $(match).closest(selectors);
                        }
                        if (elem !== undefined && elem.length === 1) {
                            return elem;
                        }
                    }
                }
                return undefined;
            };
            Element.prototype.renderInternal = function (match) {
                var ancestor = this.findAncestor(match);
                if (ancestor !== undefined) {
                    if (ancestor.hasClass(scropt.SC_CLASS_UPGRADED)) {
                        // get the element
                        this.parent = ancestor; //.data(doitElem);
                    }
                    else {
                    }
                }
                var renderRaw = this.render(match);
                if (renderRaw) {
                    return $(renderRaw);
                }
                return null;
            };
            Element.prototype.getModel = function () {
                return this.model;
            };
            Element.prototype.render = function (match) {
                return match;
            };
            ;
            return Element;
        }());
        Elements.Element = Element;
    })(Elements = scropt.Elements || (scropt.Elements = {}));
    /**
     * Services
     */
    var Services;
    (function (Services) {
        /**
         * Service, a.k.a. singleton
         * @param name the name of the service
         * @param service the service
         */
        function register(name, service) {
            if (service instanceof Service) {
                // it's a Service instance
                FrontPartStorage.addService(name, service);
            }
            else if (service.hasOwnProperty('init')) {
                // it's a simple object
                FrontPartStorage.addService(name, new ServiceWrapper(service));
            }
            else {
                // it's a Service classname
                FrontPartStorage.addService(name, new service());
            }
            //FrontPartStorage.addService(name, service);
            return this;
        }
        Services.register = register;
        function get(name) {
            return FrontPartStorage.getService(name, null);
        }
        Services.get = get;
        /**
         * Service
         */
        var Service = (function () {
            function Service() {
            }
            Service.prototype.init = function () {
                // do nothing, the devs can override it
            };
            ;
            return Service;
        }());
        Services.Service = Service;
        /**
         * A wrapper class for the interface so it can be stored like a Service instance.
         */
        var ServiceWrapper = (function (_super) {
            __extends(ServiceWrapper, _super);
            function ServiceWrapper(service) {
                _super.call(this);
                /*for(var key in service) {
                 this[key] = service[key];
                 }*/
                var keys = Object.keys(service);
                var _this = this;
                for (var i = 0; i < keys.length; i++) {
                    this[keys[i]] = service[keys[i]];
                }
            }
            return ServiceWrapper;
        }(Service));
    })(Services = scropt.Services || (scropt.Services = {}));
    var FrontPartStorage;
    (function (FrontPartStorage) {
        //var elements:Elements.Element[] = [];
        FrontPartStorage.elements = [];
        var elementsBySelector = {};
        //export var elements = {};
        var services = [];
        /**
         * @param name
         * @param elem

         export function addElement(name:string, elem:Elements.Element) {
            console.info(name, elem);
            elements[name] = elem;
        }*/
        function addElement(name, elem) {
            if (!elem.hasOwnProperty("name")) {
                elem.name = name;
                elem.getName = function () {
                    return elem.name;
                };
            }
            var holder = new ElementHolder(elem);
            // TODO [REMOVED FOR TESTING] console.debug("selector", elem.selector);
            elementsBySelector[elem.selector] = holder;
            FrontPartStorage.elements.push(holder);
        }
        FrontPartStorage.addElement = addElement;
        function addService(name, service) {
            services[name] = new ServiceHolder(service);
        }
        FrontPartStorage.addService = addService;
        function elementByName(name) {
            for (var i = 0; i < FrontPartStorage.elements.length; i++) {
                var elem = FrontPartStorage.elements[i].element;
                if (elem && elem.name === name) {
                    return FrontPartStorage.elements[i].init(); // FIXME should we really instantiate it?
                }
            }
            throw new ReferenceError("There's no element registered with the name '" + name + "'.");
        }
        FrontPartStorage.elementByName = elementByName;
        function elementBySelector(selector) {
            var elem = elementsBySelector[selector];
            if (elem === undefined) {
                throw new ReferenceError("There's no element registered with the selector '" + selector + "'.");
            }
            return (elem.init()); // FIXME should we really instantiate it?
        }
        FrontPartStorage.elementBySelector = elementBySelector;
        function getService(name, typeHack) {
            var service = services[name];
            if (service === undefined) {
                throw new ReferenceError("There's no service registered with the name '" + name + "'.");
            }
            return (service.get(null));
        }
        FrontPartStorage.getService = getService;
        var ElementHolder = (function () {
            function ElementHolder(element) {
                this.element = element;
            }
            ElementHolder.prototype.init = function () {
                var elem = this.element;
                if (elem.hasOwnProperty('render') && typeof elem !== typeof Function) {
                    // it's a simple object
                    return Object.create(elem);
                }
                else {
                    // it's a class extending Elements.Element
                    return new elem();
                }
            };
            return ElementHolder;
        }());
        /**
         * Service holder for holding the service instance and init it in case of first retrieval.
         */
        var ServiceHolder = (function () {
            function ServiceHolder(service) {
                this.initialized = false;
                this.service = service;
            }
            ServiceHolder.prototype.get = function (typeHack) {
                if (!this.initialized) {
                    this.initialized = true;
                    //var service = <T>Object.create(this.service);
                    var service = this.service;
                    service.init();
                    return service;
                }
            };
            return ServiceHolder;
        }());
    })(FrontPartStorage || (FrontPartStorage = {}));
    var A = (function (_super) {
        __extends(A, _super);
        function A() {
            _super.apply(this, arguments);
            this.replace = Elements.InsertMode.Insert;
        }
        A.prototype.render = function (match) {
            return "<p>Valami amerika</p>";
        };
        A.selector = ".a";
        return A;
    }(scropt.Elements.Element));
    var InputTest = (function (_super) {
        __extends(InputTest, _super);
        function InputTest() {
            _super.apply(this, arguments);
            this.replace = Elements.InsertMode.Wrap;
        }
        InputTest.prototype.render = function (match) {
            var attrs = DataBinding.getScroptAttributes(match);
            //console.log('attrs: ', attrs);
            //console.log('BINDING', this.binding);
            /*if (attrs['model'] !== undefined) {
             //console.log(InnerParser.varName(attrs['model']));

             // FIXME a this.model valójában a PageController-ből kell jöjjön!!!
             let model = this.model;
             //model['data'] = {input: ''};

             $(match).val(model['data']['input']);

             $(match).on('input', function () {
             model['data']['input'] = $(this).val();
             });

             this.binding.observe(attrs['model'], {
             onModelUpdate: () => {
             //console.log('UPDATE on ', attrs['model']);
             if (model['data']['input'] !== $(match).val()) {
             $(match).val(model['data']['input']);
             }
             //if($(match).val());
             },
             onModelDelete: () => {
             //console.log('DELETE on ', attrs['model']);
             }
             });
             }*/
            //match.replaceWith('<span>WOWOW</span>');
            return $("<div style=\"border: 1px solid red; padding: 12px;\"/>");
        };
        InputTest.selector = "input"; //[type='text']
        return InputTest;
    }(scropt.Elements.Element));
    //Elements.register("a", A);
    //Elements.register("input", InputTest);
    // preparing things for the framework, not working on actual data yet
    initMain();
    // rendering happens later when the page is ready
    initUI();
    $(function () {
        var model = DataBinding.Model.createModel();
        console.log('MODEL PRE', model);
        var x = model.value;
        window['MODEL'] = model;
        //model.value.a = {b: {c: {d: 23}}};
        model.observe('a.b.c', {
            onModelUpdate: function () {
                console.log('update ' + Date.now());
            },
            onModelDelete: function () {
                console.log('deleted a.b.c observer');
            }
        });
        //model.value.a = {b: {c: {d: 23}}};
        model.value.a = { b: { c: 23 } };
        //model.value.a.b.c = 111;
        //delete x.a.b.c;
        console.log('MODEL POST', model);
        /*scropt.ElementM.register('a', {
         replace: false,
         selector: '.',
         render: function () {
         return "";
         }
         });*/
        /*Elements.register("A", A);
         //Elements.register("A", new A());
         Elements.register("B", {
         render: function (match:JQuery) {

         }
         });*/
        // testing the data binder
        /* TODO [REMOVED FOR TESTING] removed tests
         var model = DataBinding.Model.createModel();

         var x = model.value;

         //var o = {};
         //var x = DataBinding.createModel(o);
         x.a = "alma";
         x.b = 238;
         x.c = {foo: "bar"};
         x.d = ["beka", "cekla"];
         x.e = {x: {y: 213, z: ["f", "g", "h"], m: {}}};

         window["X"] = x;

         // observation
         window["M"] = model;
         model.observe('a', {
         onModelUpdate: function () {
         console.log("'a' updated");
         },
         onModelDelete: function () {
         console.log("'a' deleted");
         }
         });

         model.observe('d', {
         onModelUpdate: function () {
         console.log("'d' updated");
         },
         onModelDelete: function () {
         console.log("'d' deleted");
         }
         });

         model.observe('e.x.m', {
         onModelUpdate: function () {
         console.log("'e.x.m' updated");
         },
         onModelDelete: function () {
         console.log("'e.x.m' deleted");
         }
         });*/
        // new observation method test
        // FIXME not working
        /*var model = DataBinding.Model.createModel();
         console.log('before');
         model.value.a = 42;
         model.observe("a", {
         onModelUpdate: function () {console.log("'a' updated");},
         onModelDelete: function () {console.log("'a' deleted");}
         });
         model.value.a = 5;
         console.log('after');*/
        // TODO [REMOVED FOR TESTING] grammar - databinding test
        // var store = new VarStoreImpl(model);
        //InnerParser.noReturn("e.x.z[1] = 'nope'", store);
    });
})(scropt || (scropt = {}));
