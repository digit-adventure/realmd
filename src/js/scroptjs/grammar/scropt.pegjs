{
    var GrammarStore;
    (function (GrammarStore) {
        function get(name) {
        }

        GrammarStore.get = get;
        function set(name, value) {
        }

        GrammarStore.set = set;
    })(GrammarStore || (GrammarStore = {}));

    // some initializers
    var vars = {};
    function getVar(name) {
        if (options.store) {
            return options.store.get(name);
        } else {
            return vars[name.raw]/* || false*/;
        }
    }

    function setVar(name, value) {
        if (options.store) {
            options.store.set(name, value);
        } else {
            vars[name.raw] = value;
        }
    }

    // setVar({raw:'a'}, 3);
}

/**
 * The grammar is below.
 * Compiler: http://pegjs.org
 */

Start = StartAny

StartAny = /*BoolExp / AriExpression / StartForOf / */StartNoReturn / StringExp / Assignment

StartNoReturn = aoe:(Assignment / FuncExp / BoolExp / AriExpression) op:(_";"_ (Assignment / FuncExp)?)* {
    if(!op || op[3] == null){
    	return aoe;
    }
}

StartAri = exp:AriExpression ';'? {return exp;}

StartBool = exp:BoolExp ';'? {return exp;}

StartFunc = exp:FuncExp ';'? {return exp;}

StartString = exp:StringExp ';'? {return exp;}

StartVarName = exp:VAR_NAME ';'? {return exp;}

StartForOf = v:VAR_NAME_TEXT _'of'_ a:(VAR_NAME / ARRAY) {
    if (Array.isArray(a)) {
        return {"item": v, "array": {"val": a}};
    } else {
        return {"item": v, "array": {"val": getVar(a), "var": a}};
    }
}
// END of possible start rules

Assignment
    = varA:VAR_NAME _"="_ exp:(Assignment / BoolExp / StringExp / AriExpression) {
        //console.log('varA', varA);
        //console.log('exp', exp);
        setVar(varA, exp);
        return exp;
    }
    / varA:VAR_NAME _ op:("+=" / "-=" / "**=" / "*=" / "%=" / "/=")_ exp:(Assignment / AriExpression) {
    	var a = getVar(varA);
        switch(op){
        	case "+=": a += exp; break;
            case "-=": a -= exp; break;
            case "**=": a = Math.pow(a, exp); break;
            case "*=": a *= exp; break;
            case "%=": a %= exp; break;
            case "/=": a /= exp; break;
        }

        if (a !== undefined && !isNaN(a)) {
        	setVar(varA, a);
        	return a;
        } else {
        	throw Error("'" + varA + op + exp + "' is not a numerical value.");
            return undefined;
        }
    }
	/ varA:VAR_NAME '++' _ {var oldVal = getVar(varA); setVar(varA, oldVal+1); return oldVal;}
    / varA:VAR_NAME '--' _ {var oldVal = getVar(varA); setVar(varA, oldVal-1); return oldVal;}
    / '++' varA:VAR_NAME _ {var oldVal = getVar(varA) + 1; setVar(varA, oldVal); return oldVal;}
    / '--' varA:VAR_NAME _ {var oldVal = getVar(varA) - 1; setVar(varA, oldVal); return oldVal;}

// Function

FuncExp
		= func:VAR_NAME _ '(' _ ')' {
			return {func: func, args: undefined};
		}
		/ func:VAR_NAME _ '(' vals:(FuncVal FuncValCom*) ')' {
			return {func: func, args: [vals[0]].concat(vals[1])};
		}

FuncVal
		= _ exp:Assignment _ {return exp;}
        / _ exp:BoolExp _ {return exp;}
        / _ exp:VAR_NAME _ {return exp;}
		/ _ exp:AriExpression _ {return exp;}
        / _ exp:VALUE _ {return exp;}

FuncValCom = _ ',' exp:FuncVal {return exp;}

// String
StringExp
		=	left:(BoolValue / VAR_VAL / STRING) _ "+" _ right:(StringExp / BoolExp / VAR_VAL) {return left + right;}
        /	left:(BoolValue / VAR_VAL / STRING) _ "+" _ middle:(StringExp) _ "+" _ right: StringExp {return left + (middle) + right;}
        /	exp:BoolValue _ "+" _ right:(StringExp) {return "" + exp + right;}
        /	exp:(NUMBER) _ "+" _ right:(StringExp) {return "" + exp + right;}
        /	"(" _ exp:(AriExpression) _ ")" _ "+" _ right:(StringExp) {return "" + exp + right;}
        /	STRING;

// Boolean

BoolExp = cond:BoolOr _ '?' _ a:BoolExp _ ':' _ b:BoolExp { return cond ? a : b; }
		/ BoolOr;

BoolOr
    =   left:BoolAnd _ BOOL_COMP_OR _ right:BoolOr { return left || right; }
    /   BoolAnd
    ;

BoolAnd
    =   left:BoolEq _ BOOL_COMP_AND _ right:BoolAnd { return left && right; }
    /   BoolEq
    ;

BoolEq
    =   left:BoolAri _ op:BOOL_EQ_OPS _ right:BoolEq { return (op==='==') ? left == right : left != right; }
    /	left:StringExp _ op:BOOL_EQ_OPS _ right:StringExp { return (op==='==') ? left == right : left != right; }
    /	left:AriExpression _ op:BOOL_EQ_OPS _ right:AriExpression { return (op==='==') ? left == right : left != right; }
    /   BoolAri
    ;

BoolAri
    =   left:AriExpression _ op:BOOL_ARI_OPS _ right:AriExpression {
    	switch(op){
        	case '>': return left > right;
            case '<': return left < right;
            case '<=': return left <= right;
            case '>=': return left >= right;
        }
    }
    /   BoolNeg
    ;

BoolNeg
    =   '!' _ val:BoolNeg {return !val;}
    /   BoolPar
    ;
BoolPar = '(' _ exp:BoolExp _ ')' {return exp;}  / BoolValue;

BoolValue = _ val:BOOL_VALUE _ {return val;} / _ val:VAR_VAL _ &([^\+\-*%/]+)? _ {return val;};

// Arithmetic expressions

AriExpression = head:AriTerm tail:(_ ("+" / "-") _ AriTerm)* {
    var result = head, i;

    for (i = 0; i < tail.length; i++) {
        if (tail[i][1] === "+") { result += tail[i][3]; }
        if (tail[i][1] === "-") { result -= tail[i][3]; }
    }

    return result;
}

AriTerm
    = head:AriFactor tail:(_ ("%" / "**" / "*" / "/") _ AriFactor)* {
    var result = head, i;

    for (i = 0; i < tail.length; i++) {
        switch(tail[i][1]){
            case "%":
                result %= tail[i][3];
                break;
            case "**":
                result = Math.pow(result, tail[i][3]);
                break;
            case "*":
                result *= tail[i][3];
                break;
            case "/":
                result /= tail[i][3];
                break;
        }
    }

    return result;
}

AriFactor
    = "(" _ expr:AriExpression _ ")" { return expr; }
	/ AriValue

AriValue = NUMBER / VAR_VAL

// Variable naming and value retrieval

/*VarName "vName"
    = [a-zA-Z_]+ { return text(); }
VarValue "vValue"
    = [a-zA-Z_]+ { return getVar(text()); }*/

/*VAR_NAME = (
          [a-zA-Z0-9_] ([a-zA-Z0-9_]* [a-zA-Z_]+ [a-zA-Z0-9_]*)+
        / [a-zA-Z] [a-zA-Z0-9_]+
        / [a-zA-Z_] // don't allow single numbers
    ) (VAR_OBJECT_PART / VAR_ARRAY_PART)* {return text();}; // TODO should return a reference or something to the variable
VAR_OBJECT_PART = '.' v:[a-zA-Z0-9_]+ idx:VAR_ARRAY_PART*;
VAR_ARRAY_PART = '[' ([0-9]+ / '"' [a-zA-Z0-9_]+ '"' / '\'' [a-zA-Z0-9_]+ '\'') ']';

VAR_VAL = VAR_NAME {return getVar(text());};*/

VAR_NAME
	= v:VAR_NAME_TEXT vi:(VAR_ARRAY_PART*) sub:(VAR_OBJECT_PART*) {
		var obj = {raw: text(), name:v}, ch = obj;

        if(vi.length > 0){
        	obj.idx = vi;
        }

		for(var i = 0; i < sub.length; i++){
            ch.sub = sub[i];
            ch = ch.sub;
        }
        return obj;
	}

VAR_NAME_TEXT = (
          [a-zA-Z0-9_] ([a-zA-Z0-9_]* [a-zA-Z_]+ [a-zA-Z0-9_]*)+
        / [a-zA-Z] [a-zA-Z0-9_]+
        / [a-zA-Z_] // don't allow single numbers
    ) {return text();};
VAR_OBJECT_PART "object" = '.' v:VAR_NAME_TEXT idx:VAR_ARRAY_PART* {return (idx.length > 0)?{name:v, idx: idx}:{name:v}};
VAR_ARRAY_PART "array_index" = '[' idx:(V_A_N / STRING) ']' {return idx};
V_A_N = [0-9]+ {return text()};

VAR_VAL = v:VAR_NAME {return getVar(v);};

// Base rules

OBJECT
    =   '{' _ vals:(PAIR PAIR_COM*) _ '}' {
    	console.log(vals);
        var o = vals[0];
        for(var p in vals[1]){
        	console.log('p', vals[1][p]);
            for(var a in vals[1][p]){
            	o[a] = vals[1][p][a];
            }
        }
        return o;
    }
    /   '{' _ '}' {return {};} // empty object
    ;

PAIR = _ n:StringExp _':'_ v:VALUE _ {var o = {}; o[n] = v; return o;} ;
PAIR_COM = ',' p:PAIR {return p;}

ARRAY_OLD
    =   '[' _ VALUE (_','_ VALUE)* _ ']'
    /   '[' _ ']' // empty array
    ;

ARRAY
    =   '[' _ vals:(VALUE VALUE_COM*) _ ']' {return [vals[0]].concat(vals[1]);}
    /   '[' _ ']' {return [];} // empty array
    ;

VALUE "value" = _ val:(StringExp / NUMBER / VAR_VAL / AriExpression / OBJECT / ARRAY / BOOL_VALUE / 'null' / 'undefined') _ {return val;};
VALUE_COM "valueCom" = ',' val:VALUE {return val;};

NUMBER "number" = [\-]?[0-9]+('.'[0-9]+)? ([Ee] [\+\-] [0-9]+)? {return parseFloat(text(), 10); }

BOOL_COMP_OR =  '||'; // 5
BOOL_COMP_AND = '&&'; // 6
BOOL_EQ_OPS = '==' / '!='; // 10
BOOL_ARI_OPS = '>=' / '<=' / '<' / '>'; // 11
BOOL_VALUE "bool" = ('true' / 'false') {return text() === 'true';};

STRING = '"' str:((ESC / UNICODE / [^"\\])*) '"' {return str.join("");}
		/ "'" str:((ESC / UNICODE / [^'\\])*) "'" {return str.join("");};
ESC = '\\' str:([\"\'\\/bfnrt]) {
	switch(str){
    	case 'b': return String.fromCharCode(8);
        case 'f': return String.fromCharCode(12);
        case 'n': return String.fromCharCode(10);
        case 'r': return String.fromCharCode(13);
        case 't': return String.fromCharCode(9);
    }
    return str;
};
UNICODE = '\\u' hex:(HEX HEX HEX HEX) { return String.fromCharCode(parseInt(hex.join(""), 16)); };
HEX = [0-9a-fA-F];

// Misc
_ "whitespace"
    = [ \t\n\r]*