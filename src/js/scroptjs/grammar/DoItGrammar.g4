grammar DoItGrammar;

doItValue : value;
doItAssign : ((varAssign | varInc | varDec) ';'?)+;
doItFunction : doItAssign | VAR_NAME;
/*doItFunction : NO_WS_FUNCTION;
NO_WS_FUNCTION : VAR_NAME '()';*/

doItBoolean
    :   boolCond
    ;
/*doItBoolean
    :   (BOOL_VALUE | VAR_NAME) ARI_OPS VAR_NAME
    |   VAR_NAME ARI_OPS (BOOL_VALUE | VAR_NAME)
    |   arithmetics ARI_OPS VAR_NAME
    |   VAR_NAME ARI_OPS arithmetics
    ;

boolCustom : boolValue EOF | boolPar | boolParAri | boolExp;

boolExp : boolCom | boolAri;

boolCom
    : (boolValue | boolPar | boolParAri | boolAri) BOOL_OPS boolCustom
    | boolCom BOOL_OPS boolCustom;

boolAri
    :   (arValue | arithmetics | boolParAri) ARI_OPS (arValue | arithmetics | boolParAri)
    ;

boolPar : '(' boolCustom ')';
boolParAri : '(' (arValue | arithmetics | boolParAri) ')';*/

boolValue : BOOL_VALUE | VAR_NAME;
//---------

/*s: a | b | aP | sP | boolValue;

b
    :   (a | aP | sP | boolValue) BOOL_OPS s
    |   b BOOL_OPS s
    ;
a
    :   (arValue | arithmetics | aP) COMPARATOR_OPS (arValue | arithmetics | aP)
    ;

//bP : '(' s ')';
aP : '(' (arValue | arithmetics | aP) ')';
sP : '(' s ')';*/

/*s: a | b | sP | boolValue;

b
    :   (a | sP | boolValue) BOOL_OPS s
    |   b BOOL_OPS s
    |   EOF
    ;
a
    :   (arValue | arithmetics) ARI_OPS (arValue | arithmetics)
    ;
sP : '(' s ')';*/

boolCond
    : boolOr ('?' doItBoolean ':' boolCond)?
    ;

boolOr
    :   boolOr BOOL_COMP_OR boolAnd
    |   boolAnd
    ;

boolAnd
    :   boolAnd BOOL_COMP_AND boolEq
    |   boolEq
    ;

boolEq
    :   boolEq BOOL_EQ_OPS boolAri
    |   boolAri
    ;

boolAri
    :   (arValue | arithmetics) BOOL_ARI_OPS (arValue | arithmetics)
    |   boolNeg
    ;

boolNeg
    :   '!'boolNeg
    |   boolPar
    ;
boolPar : '(' doItBoolean ')' | boolValue;

//---------
varAssign
    :   VAR_NAME '=' (value | varCond | doItBoolean)
    |   VAR_NAME '+=' (value | varCond)
    |   VAR_NAME '-=' (value | varCond);
varInc : VAR_NAME '++';
varDec : VAR_NAME '--';

varCond : (value) '?' (value | varCond) ':' (value | varCond) | varCondPar ;
varCondPar : '(' varCond ')';

// arithmetics
arithmetics: arValue EOF | arPar | arPow | arRem | arDiv | arMul | arSub | arAdd;

arAdd
    : (arValue | arPar | arPow | arRem | arDiv | arMul | arSub) '+' (arValue | arPar | arPow | arRem | arDiv | arMul | arSub)
    | arAdd '+' (arValue | arPar | arPow | arRem | arDiv | arMul | arSub)
    | (arPar | arPow | arRem | arDiv | arMul | arSub);    // addition
arSub
    : (arValue | arPar | arPow | arRem | arDiv | arMul) '-' (arValue | arPar | arPow | arRem | arDiv | arMul)
    | arSub '-' (arValue | arPar | arPow | arRem | arDiv | arMul)
    | (arPar | arPow | arRem | arDiv | arMul);    // subtraction
arMul
    : (arValue | arPar | arPow | arRem | arDiv) '*' (arValue | arPar | arPow | arRem | arDiv)
    | arMul '*' (arValue | arPar | arPow | arRem | arDiv)
    | (arPar | arPow | arRem | arDiv);    // multiplication
arDiv
    : (arValue | arPar | arPow | arRem) '/' (arValue | arPar | arPow | arRem)
    | arDiv '/' (arValue | arPar | arPow | arRem)
    | (arPar | arPow | arRem);    // division
arRem
    : (arValue | arPar| arPow) '%' (arValue | arPar| arPow)
    | arRem '%' (arValue | arPar | arPow)
    | (arPar | arPow);    // remainder
arPow
    : (arValue | arPar) '**' (arValue | arPar)
    | arPow '**' (arValue | arPar)
    | arPar;    // power
arPar
    : '(' (arithmetics | arValue ) ')';  // parentheses

arValue : VAR_NAME | NUMBER;

// primitives
object
    :   '{' pair (',' pair)* '}'
    |   '{' '}' // empty object
    ;

pair:   STRING ':' value ;

array
    :   '[' value (',' value)* ']'
    |   '[' ']' // empty array
    ;

value
    :   STRING
    |   NUMBER
    |   VAR_NAME
    |   arithmetics
    |   object  // recursion
    |   array   // recursion
    |   BOOL_VALUE  // keywords
    |   'null'
    |   'undefined'
    ;

BOOL_COMP_OR :  '||'; // 5
BOOL_COMP_AND : '&&'; // 6
BOOL_EQ_OPS : '==' | '!='; // 10
BOOL_ARI_OPS : '>=' | '<=' | '<' | '>'; // 11
BOOL_VALUE : 'true' | 'false';
VAR_NAME :
    (
          [a-zA-Z0-9_] ([a-zA-Z0-9_]* [a-zA-Z_]+ [a-zA-Z0-9_]*)+
        | [a-zA-Z] [a-zA-Z0-9_]+
        | [a-zA-Z_]
    ) VAR_OBJECT_PART*; // don't allow single numbers
fragment VAR_OBJECT_PART : '.' [a-zA-Z0-9_]+ VAR_ARRAY_PART*;
fragment VAR_ARRAY_PART : '[' (INT | '"' [a-zA-Z0-9_]+ '"' | '\'' [a-zA-Z0-9_]+ '\'') ']';

STRING :  ('\'' | '"') (ESC | ~["\\])* ('\'' | '"');
fragment ESC :   '\\' (["\\/bfnrt] | UNICODE) ;
fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment HEX : [0-9a-fA-F] ;
NUMBER
    :   '-'? INT '.' [0-9]+ EXP? // 1.35, 1.35E-9, 0.3, -4.5
    |   '-'? INT EXP             // 1e10 -3e4
    |   '-'? INT                 // -3, 45
    ;
fragment INT :   '0' | [1-9] [0-9]* ; // no leading zeros
fragment EXP :   [Ee] [+\-]? INT ; // \- since - means "range" inside [...]
WS  :   [ \t\n\r]+ -> skip ;